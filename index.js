/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { YellowBox } from 'react-native';
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import reducer from './component/redux/reducers'

YellowBox.ignoreWarnings(['Remote debugger']);

const store = createStore(reducer);

const AppContainer  = () => 
    <Provider store={store}>
        <App/>
    </Provider>

store.subscribe( () => {
    console.log("State has changed"  + store.getState().toString());
})

AppRegistry.registerComponent(appName, () => AppContainer );
