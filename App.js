import React from 'react';
import FrmNuovaDicussione from './component/form/frmNuovaDiscussione';
import FrmQuestion from './component/form/tabTrova/tabQuestion';
import FrmCondividi from './component/form/frmCondividi';
import FrmHomeScreen from './component/form/frmHomeScreen';
import FrmCerca from './component/form/tabCerca/navCerca';

import AuthLoadingScreen from './component/form/AuthLoadingScreen';
import WelcomeScreen from './component/form/WelcomeScreen';
import SignInScreen from './component/form/SignInScreen';
import LoginScreen from './component/form/LoginScreen';

import { createStackNavigator, createSwitchNavigator, createMaterialTopTabNavigator } from 'react-navigation';

const RootStack = createStackNavigator({
  navHome: {
    screen: FrmHomeScreen,
  },
  navDiscussione: {
    screen: FrmNuovaDicussione,
  },
  navTrova: {
    screen: FrmQuestion,
  },
  navCondividi: {
    screen: FrmCondividi,
  },  
  navCerca: {
    screen: FrmCerca,
  },
});

const AuthStackNavigator = createStackNavigator({
  Welcome: {
    screen: WelcomeScreen,
    header: null,
    navigationOptions: {
        header: null
    } 
  }, 
  SignIn: {
    screen: SignInScreen,
    header: null,
    navigationOptions: {
        header: null
    }     
  },
  Login: {
    screen: LoginScreen,
    header: null,
    navigationOptions: {
        header: null
    }     
  }
});

export default createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Auth: AuthStackNavigator,
  App: RootStack,
})



