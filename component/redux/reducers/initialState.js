const initialState = {
    profilo: {
        profileList: [],
        sPathImmagine: null,
        sNome: '',
        sCognome: '',
        sMotto: '',
        iInteresse: 0,
        sInteresse: '',

    },
    ui: {
        isContactFormHidden: true
    }
}

export default initialState;