import initialState from './initialState';
import { EDT_PROFILO } from './../types';

export default (state = initialState.profilo, action) => {
    // console.log(state);
    console.log(action);
    switch (action.type) {
        case EDT_PROFILO:
            console.log('1');
            state = Object.assign({}, state, {
                    sPathImmagine: action.payload.profilo.sPathImmagine,
                    sNome: action.payload.profilo.sNome,
                    sCognome: action.payload.profilo.sCognome,
                    sMotto: action.payload.profilo.sMotto,
                    iInteresse: action.payload.profilo.iInteresse,
                    sInteresse: action.payload.profilo.sInteresse,

            });
            break;
    }
    return state;
}