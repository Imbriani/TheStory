import { combineReducers } from 'redux'
import profileReducer from './reducerProfilo';
import uiReducer from './reducerUI';
 
export default combineReducers({
     
    profilo: profileReducer,
    ui: uiReducer,
   
});