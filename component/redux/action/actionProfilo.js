import { EDT_PROFILO } from './../types';

export const editProfilo =(profilo) => {
    return {
        type: EDT_PROFILO,
        payload: { profilo}
    }    
}