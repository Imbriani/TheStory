import React, { Component } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, Dimensions, ScrollView, Keyboard } from 'react-native';
import { Container, Text, View, Icon, Button } from "native-base";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Autocomplete from 'react-native-autocomplete-input';

// const API = 'https://swapi.co/api';
// const API = 'https://api.themoviedb.org/3/movie/550?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT';


// Nascondere la scelta del tipo di filtro e impostare il testo del filtro quando viene chiamato dalla form delle 
// discussioni filtrate per una storia

export default class frmNewDiscussione extends Component {

    constructor(props) {
        super(props);
        this.state = {
            idxTipoDisc: 5,
            sTestoFiltro: 'Inserisci il titolo di un Film',
            films: [],
            sApi: 'https://api.themoviedb.org/3/search/movie?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=',
            sRif: 'title'
        };
        console.log('cons');
    }

    static navigationOptions = {
        title: 'Discuti su',
        headerTintColor: '#ffffff',
        headerStyle: {
            backgroundColor: '#3F51B5',
        },
        headerTitleStyle: {
            fontSize: 20,
            fontFamily: 'GoogleSans-Regular',
        },
    };

    componentDidMount() {
        //     this.keyboardDidShowListener =
        //     Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        //   this.keyboardDidHideListener =
        //     Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);        
        // fetch('https://api.themoviedb.org/3/movie/550?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT').then(res => res.json()).then((json) => {
        //     const { results: films } = json;
        //     this.setState({ films });
        //     console.log('fetch1');
        // });
        // console.log(API);

        // console.log('dids');
        // return fetch('https://api.themoviedb.org/3/search/movie?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&query=gat&page=1&include_adult=false')
        //     .then((response) => response.json())
        //     .then((responseJson) => {
        //         console.log(responseJson);
        //         this.setState({
        //             films: responseJson,
        //         }, function () {                    
        //         });
        //     })
        //     .catch((error) => {
        //         console.error(error);
        //     });              

    }

    // componentWillUnmount() {
    //     this.keyboardDidShowListener.remove();
    //     this.keyboardDidHideListener.remove();
    // }

    // _keyboardDidShow = (e) => {
    //     let listHeight =
    //       Dimensions.get('window').height - e.endCoordinates.height;
    //     this.setState({
    //       listHeight,
    //     });
    //     console.log(listHeight);
    //   }

    // findFilm(query) {
    //     console.log(query);
    //     if (query === '') {
    //         return [];
    //     }

    //     const { films } = this.state;
    //     console.log(films);
    //     const regex = new RegExp(`${query.trim()}`, 'i');
    //     console.log(regex);
    //     return films.results.filter(film => film.title.search(regex) >= 0);
    // }


    selezionaTipo = (iIdx, sTesto) => () => {
        this.setState({
            idxTipoDisc: iIdx,
            sTestoFiltro: sTesto,
            query: '',
            films: [],
        });
        console.log(this.state.query);
        switch (iIdx) {
            case 5:
                this.setState({ sApi: 'https://api.themoviedb.org/3/search/movie?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=', });
                break;
            case 6:
                this.setState({ sApi: 'https://api.themoviedb.org/3/search/tv?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=', });
                break;
            case 1:
                this.setState({ sApi: 'https://api.themoviedb.org/3/search/person?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=', });
                break;
            case 3:
                this.setState({ sApi: 'https://api.themoviedb.org/3/search/person?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=', });
                break;
        }
        console.log(this.state.sApi);
    }

    render() {
        const { query } = this.state;

        return (

            <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }} style={{ flex: 1 }} scrollEnabled={false}>
                <Container>
                    <View style={styles.vwScroll}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ alignContent: 'center' }}>
                            <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 5 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(5, 'Inserisci il titolo di un Film')}>
                                <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 5 ? styles.txtButtonToolsSel : {}]}>Film</Text>
                            </Button>
                            <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 6 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(6, 'Inserisci il titolo di una Serie TV')}>
                                <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 6 ? styles.txtButtonToolsSel : {}]}>Serie TV</Text>
                            </Button>
                            <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 1 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(1, 'Inserisci il nome di un Attore')}>
                                <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 1 ? styles.txtButtonToolsSel : {}]}>Attore</Text>
                            </Button>
                            <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 2 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(2, 'Inserisci il nome di uno Scrittore')}>
                                <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 2 ? styles.txtButtonToolsSel : {}]}>Scrittore</Text>
                            </Button>
                            <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 3 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(3, 'Inserisci il nome di un Regista')}>
                                <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 3 ? styles.txtButtonToolsSel : {}]}>Regista</Text>
                            </Button>
                            <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 4 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(4, 'Inserisci il titolo di un Libro')}>
                                <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 4 ? styles.txtButtonToolsSel : {}]}>Libro</Text>
                            </Button>
                            <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 4 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(7, 'Inserisci il titolo di una TS')}>
                                <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 7 ? styles.txtButtonToolsSel : {}]}>TS</Text>
                            </Button>                            
                        </ScrollView>
                    </View>

                    <View style={styles.vwFiltro}>

                    </View>

                    <View style={styles.vwTitolo}>
                        <TextInput style={styles.txtTitolo} placeholder='Titolo'>
                        </TextInput>
                    </View>

                    <View style={styles.vwTesto}>
                        <TextInput style={styles.txtTesto} multiline={true} placeholder='Testo/link...'>

                        </TextInput>
                    </View>
                    <View style={styles.vwInfo}>
                        <Text style={styles.textInfo}>Usa gli YO! per premiare le risposte migliori</Text>
                    </View>
                   
                   <View style={styles.vwButton}>
                        <View style={{ position: 'absolute', top: 7 }}>
                            <Button rounded style={styles.btnPubblica}>
                                <Text style={styles.txtButton} uppercase={false}>Apri Discussione</Text>
                            </Button>
                        </View>
                    </View>

                    <Autocomplete                        
                        autoCapitalize="none"
                        autoCorrect={false}
                        data={this.state.films.results}
                        defaultValue={query}
                        placeholder={this.state.sTestoFiltro}
                        containerStyle={styles.autocompleteContainer}
                        inputContainerStyle={{ borderColor: '#fff' }}
                        style={{ fontSize: 18, fontFamily: 'GoogleSans-Regular' }}
                        listStyle={styles.list}
                        onPress={() => { console.log('press') }}
                        renderItem={({ title, name, release_date }) => (
                            <TouchableOpacity onPress={() => { this.setState({ query: '' }); this.setState({ query: title }); console.log(title); this.setState({ films: [] }) }}>
                                <Text style={styles.itemText}>
                                    {title} {name} 
                                    {/* {this.state.idxTipoDisc === 5 ? ({release_date}) : null}  */}
                                </Text>
                            </TouchableOpacity>
                        )}
                        onChangeText={text =>
                        fetch(`${this.state.sApi}${text}`)
                            .then((response) => response.json())
                            .then((responseJson) => {
                                console.log(responseJson);
                                this.setState({
                                    films: responseJson,
                                }, function () {
                                });
                            })
                            .catch((error) => {
                                console.error(error);
                            })
                    }
                    />
                </Container>
            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({
    list: {
        paddingRight: 7,
        paddingLeft: 7,
        margin: 0,
        borderWidth: 0,
        maxHeight: 270, // necessary to make scrolling of list possible see:
    },
    itemText: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
    },
    autocompleteContainer: {
        flex: 1,
        left: 5,
        position: 'absolute',
        right: 5,
        top: 46,
        zIndex: 1,
    },
    text: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        margin: 10,
    },
    textInfo: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        margin: 10,
        color: '#A9A9A9'
    },
    vwScroll: {
        height: 40,
        flexDirection: 'row',
        borderBottomColor: '#7a42f4',
        borderBottomWidth: 1,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
    },
    vwIntestazione: {
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#7a42f4',
        marginLeft: 5,
        marginRight: 5,
    },
    txtTag: {
        fontFamily: 'GoogleSans-Regular',
        fontSize: 18,
    },
    vwTitolo: {
        flex: .5,
        // borderBottomWidth: 1,
    },
    vwFiltro: {
        flex: .5,
        borderBottomColor: '#7a42f4',
        borderBottomWidth: 1,
        marginLeft: 5,
        marginRight: 5,
        // borderBottomWidth: 1,
    },
    txtTitolo: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        justifyContent: 'flex-start',
        borderBottomColor: '#7a42f4',
        borderBottomWidth: 1,
        marginLeft: 5,
        marginRight: 5,
        paddingLeft: 10,
        paddingRight: 10,
    },
    vwTesto: {
        flex: 3,
        borderBottomWidth: 1,
        borderBottomColor: '#7a42f4',
        marginLeft: 5,
        marginRight: 5,
    },
    txtTesto: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        justifyContent: 'flex-start',
        paddingLeft: 10,
        paddingRight: 10,
    },
    vwInfo: {
        flex: .5,
        borderBottomWidth: 1,
        borderBottomColor: '#7a42f4',
    },
    vwButton: {
        flex: 1,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
        minHeight: 20,
    },
    btnPubblica: {
        width: 200,
        height: 50,
        justifyContent: 'center',
    },
    txtButton: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
    },
    vwButtonImg: {
        flex: .8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    txtImmagine: {
        fontSize: 18,
        color: '#A9A9A9',
        fontFamily: 'GoogleSans-Regular',
    },
    btnTools: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
    },
    btnToolsSel: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: 'rgba(63,81,181, .9)',
    },
    txtButtonTools: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        color: '#cdcdcd',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    txtButtonToolsSel: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
});
