import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, AsyncStorage } from 'react-native';
import { Container, Content, Text, List, ListItem, Separator } from "native-base";

export default class tabHome extends Component {

    constructor(props) {
        super(props);
    }

    // non più usato
    // handlePress = (e) => {
    //     if (typeof this.props.onPress === 'function') {
    //       this.props.onPress()
    //     }
    // }    

    render() {
        return (
            <Container>
                <Content>
                    <Separator bordered />
                    <List>
                        <ListItem button={true} >
                            <Text style={styles.Text}>Acquista storie vincitrici</Text>
                        </ListItem>
                        <ListItem button={true} >
                            <Text style={styles.Text}>I miei ebook</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>I miei audio libri</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Lista desideri</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Converti i tuoi YO</Text>
                        </ListItem>
                    </List>
                    <Separator bordered />
                    <List>
                        <ListItem button={true} >
                            <Text style={styles.Text}>Cinema news</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Book news</Text>
                        </ListItem>
                    </List>
                    <Separator bordered />
                    <List>
                        <ListItem>
                            <Text style={styles.Text}>Oggi in TV</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Questa settimana al cinema</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Questa settimana in libreria</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Classifiche settimanali</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Top list</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Seguiti</Text>
                        </ListItem>
                        <Separator bordered />
                        <List>
                            <ListItem>
                                <Text style={styles.Text}>Legenda punti Yo</Text>
                            </ListItem>
                            <ListItem>
                                <Text style={styles.Text}>Regole The Story</Text>
                            </ListItem>
                        </List>
                    </List>
                    <Separator bordered />
                    <List>
                        <ListItem>
                            <Text style={styles.Text}>Filtro spoiler</Text>
                        </ListItem>
                    </List>
                    <Separator bordered />
                    <List>
                        <ListItem>
                            <TouchableOpacity onPress={this.props.onPress}>
                                <Text style={styles.Text}>Logout</Text>
                            </TouchableOpacity>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Segnala un problema</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.Text}>Privacy e sicurezza</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    Text: {
        fontSize: 18,
        textAlign: 'center',
        fontFamily: 'GoogleSans-Regular',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
