import React, { Component } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, Slider, ScrollView } from 'react-native';
import { Container, Text, View, Icon, Button } from "native-base";
import Modal from 'react-native-modalbox';
import DataScreen from './modData';
import FiltroFilmScreen from './modFiltroFilm';
import GenereScreen from './modGenere';
import FiltroAttoriScreen from './modFiltroAttori';

export default class frmNewDiscussione extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bPulsanteSalva: true,
            sTestoAttore: 'Attore/Scrittore/Regista',
            sTestoAnno: 'Anno',
            sTestoSimile: 'Simile a',
            sTestoGenere: 'Genere',
        };
    }

    ChiudiData = (s, s2) => () => {
        this.refs.modData.close();
        console.log(s);
        console.log(s2);
        this.setState({
            sTestoAnno: s,  
        }); 
    }
    AnnullaData = () => {
        this.refs.modData.close(); 
    }  

    ChiudiFiltroAttori = (s) => () => {
        this.refs.modAttori.close();
        this.setState({
            sTestoAttore: s,
        }); 
    }
    AnnullaFiltroAttori= () => {
        this.refs.modAttori.close();
    }   

    ChiudiFiltroFilm = (s) => () => {
        this.refs.modSimile.close();
        this.setState({
            sTestoSimile: s,
        }); 
    }
    AnnullaFiltroFilm = () => {
        this.refs.modSimile.close();
    }

    ChiudiGenere = (s) => () => {
        this.refs.modGenere.close();
        this.setState({
            sTestoGenere: s,
        }); 
    }
    AnnullaGenere = () => {
        this.refs.modGenere.close(); 
    }  

    ResetTestoAttori = () => this.setState(state => ({
        sTestoAttore: 'Attore/Scrittore/Regista'
    }));
    ResetTestoAnno = () => this.setState(state => ({
        sTestoAnno: 'Anno'
    }));
    ResetTestoSimile = () => this.setState(state => ({
        sTestoSimile: 'Simile a'
    }));   
    ResetTestoGenere = () => this.setState(state => ({
        sTestoGenere: 'Genere'
    })); 

    render() {
        return (
            <Container>
                <View style={styles.vwDettagli}>
                    <Text style={styles.textDettagli}>Dettagli</Text>
                </View>

                <View style={styles.vwFiltro}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 5 }} onPress={() => { this.refs.modGenere.open(); this.setState({ bPulsanteSalva: false }); console.log('no publ')}}>
                        <Text style={styles.txtAnno}>{this.state.sTestoGenere}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, marginRight: 5 }} onPress={this.ResetTestoGenere}>
                        <Icon name='md-close-circle' style={styles.iconExit}/>
                    </TouchableOpacity>
                </View>

                <View style={styles.vwFiltro}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 5 }} onPress={() => { this.refs.modAttori.open(); this.setState({ bPulsanteSalva: false }); console.log('no publ')}}>
                        <Text style={styles.txtAnno}>{this.state.sTestoAttore}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, marginRight: 5 }} onPress={this.ResetTestoAttori}>
                        <Icon name='md-close-circle' style={styles.iconExit}/>
                    </TouchableOpacity>
                </View>

                <View style={styles.vwAnno}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 5 }} onPress={() => { this.refs.modData.open(); this.setState({ bPulsanteSalva: false }); console.log('no publ') }}>
                        <Text style={styles.txtAnno}>{this.state.sTestoAnno}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, marginRight: 5 }} onPress={this.ResetTestoAnno}>
                        <Icon name='md-close-circle' style={styles.iconExit}/>
                    </TouchableOpacity>
                </View>

                <View style={styles.vwFiltroSimile}>
                    <TouchableOpacity style={{ justifyContent: 'center', marginTop: 7 }} onPress={() => { this.refs.modSimile.open(); this.setState({ bPulsanteSalva: false });console.log('no publ') }}>
                        <Text style={styles.txtAnno}>{this.state.sTestoSimile}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, marginRight: 5 }} onPress={this.ResetTestoSimile}>
                        <Icon name='md-close-circle' style={styles.iconExit}/>
                    </TouchableOpacity>                    
                </View>

                <View style={[styles.vwButton]}>
                    {this.state.bPulsanteSalva === true ?
                        <Button rounded style={styles.btnPubblica} onPress={() => this.props.navigation.navigate('TabRisultatiTS')}>
                            <Text style={styles.txtButton} uppercase={false}>Trova</Text>
                        </Button>
                        : null
                    }
                </View>

                <Modal style={[styles.modalData]} position={"top"} swipeToClose={false} ref={"modData"} onClosed={() => { this.setState({ bPulsanteSalva: true }) }}>
                    <DataScreen onPress={this.ChiudiData} onAnnulla={this.AnnullaData} />
                </Modal>

                <Modal style={[styles.modalFiltro]} position={"top"} swipeToClose={false} ref={"modAttori"} onClosed={() => { this.setState({ bPulsanteSalva: true }) }}>
                    <FiltroAttoriScreen onPress={this.ChiudiFiltroAttori} onAnnulla={this.AnnullaFiltroAttori} />
                </Modal>

                <Modal style={[styles.modalGenere]} position={"top"} swipeToClose={false} ref={"modGenere"} onClosed={() => { this.setState({ bPulsanteSalva: true }) }}>
                    <GenereScreen onPress={this.ChiudiGenere} onAnnulla={this.AnnullaGenere}/>
                </Modal>           

                <Modal style={[styles.modalFiltro]} position={"top"} swipeToClose={false} ref={"modSimile"} onClosed={() => { this.setState({ bPulsanteSalva: true }) }}>
                    <FiltroFilmScreen onPress={this.ChiudiFiltroFilm} onAnnulla={this.AnnullaFiltroFilm}/>
                </Modal>                        
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    modalData: {
        // flex: 1,
        flexDirection: 'column',
        borderWidth: .4,
        borderColor: 'rgb(47,79,79)',
        backgroundColor: '#FFFFFF',
        width: 330,
        height: 350,
        marginTop: 5,
        // bottom: 550,
        position: 'absolute',
        zIndex: 1,
    },
    modalGenere: {
        // flex: 1,
        flexDirection: 'column',
        borderWidth: .4,
        borderColor: 'rgb(47,79,79)',
        backgroundColor: '#FFFFFF',
        width: 330,
        height: 350,
        marginTop: 5,
        // bottom: 550,
        position: 'absolute',
        zIndex: 1,
    },
    modalFiltro: {
        flex: 1,
        flexDirection: 'column',
        borderWidth: .4,
        borderColor: 'rgb(47,79,79)',
        backgroundColor: '#FFFFFF',
        width: 330,
        // height: 350,
        // marginTop: 5,
        top: 10,
        // bottom: 490,
        marginBottom: 20,
        position: 'absolute',
    },
    list: {
        paddingRight: 7,
        paddingLeft: 7,
        margin: 0,
        borderWidth: 0,
        maxHeight: 270, // necessary to make scrolling of list possible see:
    },
    btnTools: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
        flex: 1
    },
    btnToolsSel: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: 'rgba(63,81,181, .9)',
    },
    txtButtonTools: {
        fontSize: 17,
        fontFamily: 'GoogleSans-Regular',
        color: '#cdcdcd',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    txtButtonToolsSel: {
        fontSize: 17,
        fontFamily: 'GoogleSans-Bold',
        color: '#fff',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    itemText: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
    },
    autocompleteContainer: {
        flex: 1,
        left: 5,
        position: 'absolute',
        right: 5,
        top: 130,
        zIndex: 2,
    },
    autocompleteContainerSimile: {
        flex: 1,
        left: 5,
        position: 'absolute',
        right: 5,
        top: 265,
        zIndex: 2,
    },
    text: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        margin: 10,
    },
    textInfo: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        margin: 10,
        color: '#A9A9A9'
    },
    vwIntestazione: {
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#7a42f4',
        marginLeft: 5,
        marginRight: 5,
    },
    txtTag: {
        fontFamily: 'GoogleSans-Regular',
        fontSize: 18,
    },
    vwAnno: {
        flex: .3,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#7a42f4',
        marginLeft: 5,
        marginRight: 5,
    },
    vwFiltro: {
        flex: .3,
        flexDirection: 'row',
        borderBottomColor: '#7a42f4',
        borderBottomWidth: 1,
        marginLeft: 5,
        marginRight: 5,
    },
    vwFiltroSimile: {
        flex: .3,
        flexDirection: 'row',
        borderBottomColor: '#7a42f4',
        borderBottomWidth: 1,
        marginLeft: 5,
        marginRight: 5,
    },
    vwDettagli: {
        flex: .2,
        borderBottomColor: '#7a42f4',
        borderBottomWidth: 1,
        marginLeft: 5,
        marginRight: 5,
        alignItems: 'center',
        justifyContent: 'center'
        // borderBottomWidth: 1,
    },
    vwGenere: {
        flexDirection: 'row',
        borderBottomColor: '#7a42f4',
        borderBottomWidth: 1,
        alignItems: 'center',
        marginLeft: 5,
        marginRight: 5,
        flex: .3,
    },
    vwScroll: {
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        // marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
    },
    textDettagli: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        margin: 10,
        color: '#000'
    },
    txtGenere: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        justifyContent: 'flex-start',
        borderBottomColor: '#7a42f4',
        marginLeft: 5,
    },
    txtAnno: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        justifyContent: 'flex-start',
        marginTop: 3,
        marginBottom: 3,
        marginLeft: 5,
    },
    iconExit: {
        position: 'absolute',
        fontSize: 35,
        right: 5,
        top: 7,
        color: '#CED0CE',
        alignItems: 'flex-end',
    },    
    vwTesto: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#7a42f4',
        marginLeft: 5,
        marginRight: 5,
    },
    txtTesto: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        justifyContent: 'flex-start',
        paddingLeft: 10,
        paddingRight: 10,
    },
    vwInfo: {
        flex: .5,
        borderBottomWidth: 1,
        borderBottomColor: '#7a42f4',
    },
    vwButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    btnPubblica: {
        width: 200,
        height: 50,
        justifyContent: 'center',
        position: 'absolute',
        bottom: 10,
        zIndex: 1,
    },
    txtButton: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
    },
    vwButtonImg: {
        flex: .8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    txtImmagine: {
        fontSize: 18,
        color: '#A9A9A9',
        fontFamily: 'GoogleSans-Regular',
    },
});
