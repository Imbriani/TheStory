import React from 'react';
import tabTrovaScreen from './ChiediTSScreen';
import ChiediCommunity from './ChiediComunityScreen';
import RisultatiTS from './RisultatiTSScreen';
import CercaFilmScreen from './modFiltroFilm'

import { createStackNavigator } from 'react-navigation';

export default createStackNavigator({
    tabTrova: {
        screen: tabTrovaScreen,        
        header: null,
        navigationOptions: {
            header: null
        }  
    },
    TabRisultatiTS: {
        screen: RisultatiTS,
        header: null,
        navigationOptions: {
            header: null
        }
    },    
    TabChiediCommunity: {
        screen: ChiediCommunity,
        header: null,
        navigationOptions: {
            header: null
        }
    },
    CercaFilm: {
        screen: CercaFilmScreen,
        header: null,
        navigationOptions: {
            header: null
        }
    }
})