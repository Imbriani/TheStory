import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image } from 'react-native';
import { Container, Content, Card, CardItem, Thumbnail, Icon, Text, Footer, FooterTab, Button, ScrollableTab, Title, Body, Left, Right } from 'native-base';

export default class risultatiTS extends Component {

    static navigationOptions = {
        title: 'Cerca un',
        headerTintColor: '#ffffff',
        headerStyle: {
            backgroundColor: '#3F51B5',
        },
        headerTitleStyle: {
            fontSize: 20,
            fontFamily: 'GoogleSans-Bold',
        }
    };

    render() {
        return (
            <Container>
                    <View style={{ height: 200, flex: 1,  borderBottomWidth: 1, borderBottomColor: '#7a42f4', marginLeft: 5, marginRight: 5 }}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ marginTop: 3 }}>
                            <View style={styles.DiscRilevanti}>
                                <View style={styles.vwTopRilevanti} >
                                    <Text style={styles.CardTextHeaderUtente}>Venom (2018)</Text>
                                </View>

                                <View style={styles.vwTxtDescRilevanti} >
                                    <Image source={{uri:'https://image.tmdb.org/t/p/w600_and_h900_bestv2/1d8ftx6u52ZszwelP3xfl74x7t6.jpg'}} style={{ width: '100%', height: '100%' }} />
                                </View>

                                <View style={styles.vwBottomRilevanti}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../../assets/img/yo.png')} style={{ width: 30, height: 30 }} />
                                        <Text style={styles.txtBtnUnisciti}>YO!</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.DiscRilevanti}>
                                <View style={styles.vwTopRilevanti} >
                                    <Text style={styles.CardTextHeaderUtente}>Ant-Man and the Wasp</Text>
                                </View>

                                <View style={styles.vwTxtDescRilevanti} >
                                    <Image source={{uri:'https://image.tmdb.org/t/p/w600_and_h900_bestv2/or1icPybZ3bcWh5rqQQH0bx8NlV.jpg'}} style={{ width: '100%', height: '100%' }} />
                                </View>

                                <View style={styles.vwBottomRilevanti}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../../assets/img/yo.png')} style={{ width: 30, height: 30 }} />
                                        <Text style={styles.txtBtnUnisciti}>YO!</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.DiscRilevanti}>
                                <View style={styles.vwTopRilevanti} >
                                    <Text style={styles.CardTextHeaderUtente}>First Man</Text>
                                </View>

                                <View style={styles.vwTxtDescRilevanti} >
                                    <Image source={{uri:'https://image.tmdb.org/t/p/w600_and_h900_bestv2/qPZa27s70mFOTNVcRtlIf4RnDUv.jpg'}} style={{ width: '100%', height: '100%' }} />
                                </View>

                                <View style={styles.vwBottomRilevanti}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../../assets/img/yo.png')} style={{ width: 30, height: 30 }} />
                                        <Text style={styles.txtBtnUnisciti}>YO!</Text>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                <View style={{ flex: .5  }}>
                    <View style={styles.vwInfo}>
                        <Text style={styles.textInfo}>Usa gli YO! per farci sapere quale film hai scelto</Text>
                    </View>
                    <View style={styles.vwInfo}>
                        <Text style={styles.textInfo}>Non sei soddisfatto delle nostre proposte?</Text>
                    </View>
                    <View style={styles.vwButton}>
                        <View style={{ position: 'absolute', top: 7 }}>
                            <Button rounded style={styles.btnPubblica} onPress={() => this.props.navigation.navigate('TabRisultatiTS')}>
                                <Text style={styles.txtButton} uppercase={false}>Chiedi alla Community</Text>
                            </Button>
                        </View>
                    </View>
                </View>

            </Container >
        );
    }
}

const styles = StyleSheet.create({
    DiscRilevanti: {
        borderWidth: 1,
        margin: 10,
        width: 200,
        borderRadius: 15,
        flex: 3,
        flexDirection: 'column',
    },
    vwTxtHeadRilevanti: {
        height: 25,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtHeadRilevanti: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        alignItems: 'center',
        marginLeft: 3,
        marginBottom: 3,
        flexDirection: 'row',
        flex: 1,
    },
    vwTopRilevanti: {
        top: 0,
        left: 0,
        right: 0,
        height: 10,
        backgroundColor: '#1565c0',
        borderWidth: 2,
        borderColor: '#1565c0',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        flex: .9,
        flexDirection: 'column',
    },
    vwTxtDescRilevanti: {
        flex: 3,
        flexDirection: 'column',
    },
    txtDescRilevanti: {
        fontSize: 12,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 7,
        marginTop: 5,
    },
    vwBottomRilevanti: {
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        borderTopWidth: 1,
        flex: .7,
        flexDirection: 'column',
        // backgroundColor: '#fff',
    },
    CardTextHeaderUtente: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'left',
        marginLeft: 7,
        marginRight: 7,
        color: '#fff',
        marginTop: 3,
    },
    CardTextHeader: {
        fontSize: 12,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 7,
        marginRight: 7,
        color: '#fff',
        maxHeight: 30,
        minHeight: 30,
    },
    vwButton: {
        flex: .15,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
        minHeight: 20,
    },
    btnPubblica: {
        width: 250,
        height: 50,
        justifyContent: 'center',
    },
    txtButton: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
    },
    vwInfo: {
        flex: .25,
    },
    textInfo: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        margin: 10,
        color: '#A9A9A9'
    },
    txtBtnUnisciti: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'left',
        marginTop: 5,
        marginLeft: 10,
    },
});
