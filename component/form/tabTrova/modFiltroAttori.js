import React, { Component } from 'react';
import { StyleSheet, FlatList, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native';
import { Container, Separator, Text, List, ListItem, View, Icon, Button } from "native-base";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class modFiltro extends Component {

    constructor(props) {
        super(props);
        this.state = {
            persone: [],
            sApiPerson: 'https://api.themoviedb.org/3/search/person?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=',
        };
        console.log('cons');
    }

    render() {

        return (
            <KeyboardAwareScrollView
            automaticallyAdjustContentInsets={false}
            bounces={false}
            showsVerticalScrollIndicator={false}
          >
                <Container>                
                    <View style={{ flex: .4 }}>
                        <TextInput placeholder='Attore/Scrittore/Regista' style={styles.txtInput}
                            onChangeText={text =>
                                fetch(`${this.state.sApiPerson}${text}`)
                                    .then((response) => response.json())
                                    .then((responseJson) => {
                                        console.log(responseJson);
                                        this.setState({
                                            persone: responseJson,
                                        }, function () {
                                        });
                                    })
                                    .catch((error) => {
                                        console.error(error);
                                    })
                            }>
                        </TextInput>
                    </View>
                    <View style={{ flex: 3 }}>
                        <FlatList
                            keyboardShouldPersistTaps= 'always' 
                            data={this.state.persone.results}
                            renderItem={({ item }) =>
                                <View>
                                    <TouchableOpacity onPress={this.props.onPress(item.name)}>
                                        <Text style={styles.txtDescrizione}>{item.name}</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                            ItemSeparatorComponent={() =>
                                <View
                                    style={{
                                        height: 2,
                                        backgroundColor: "#CED0CE",
                                    }}
                                />
                            }
                            keyExtractor={item => item.id}
                        />
                    </View>

                    <View style={{ flex: .3, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center' }}>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center', position: 'absolute' }}>
                            <Button rounded style={styles.btnSalva} onPress={this.props.onAnnulla} >
                                <Text style={styles.txtButton} uppercase={false}>Annulla.</Text>
                            </Button>
                        </View>
                    </View>
                </Container>
            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({

    Text: {
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'GoogleSans-Bold',
    },
    txtInput: {
        fontSize: 15,
        textAlign: 'left',
        justifyContent: 'center',
        fontFamily: 'GoogleSans-Regular',
        margin: 4,
        borderWidth: 1,
        borderRadius: 20,
        borderColor: '#7a42f4',
        paddingHorizontal: 10,
    },
    txtDescrizione: {
        fontSize: 16,
        textAlign: 'left',
        justifyContent: 'center',
        fontFamily: 'GoogleSans-Regular',
        margin: 7,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    iconText: {
        fontSize: 30,
        color: '#808080'
    },
    btnSalva: {
        width: 100,
        height: 30,
        bottom: 10,
        alignItems: 'flex-end',
        flex: 1,
        justifyContent: 'flex-end',
        marginLeft: 5,
        marginRight: 5,
    },
    txtButton: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
        bottom: 2,
    },
    iconExit: {
        position: 'absolute',
        fontSize: 25,
        right: 7,
        top: 7,
        color: '#808080',
        flex: 1,
        alignItems: 'flex-end',
    },
    list: {
        paddingRight: 7,
        paddingLeft: 7,
        margin: 0,
        borderWidth: 0,
        maxHeight: 270, // necessary to make scrolling of list possible see:
    },
    itemText: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
    },
});
