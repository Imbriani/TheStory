import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, AsyncStorage, ScrollView } from 'react-native';
import { Container, Header, Tabs, Tab, TabHeading, Icon, Text, Footer, FooterTab, Button, ScrollableTab, Title, Body, Left, Right } from 'native-base';
import TabChiediTS from './navChiediTS';
import TabChiediCommunity from './ChiediComunityScreen';

export default class tabQuestion extends Component {

  constructor(props) {
    super(props);
    this.state = {
      idxTipoDisc: 1,
      sTestoFiltro: 'Inserisci il titolo di un Film',
    };
  }
  static navigationOptions = {
    title: 'Trova',
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#3F51B5',
    },
    headerTitleStyle: {
      fontSize: 20,
      fontFamily: 'GoogleSans-Regular',
    },
  };

  selezionaTipo = (iIdx, sTesto) => () => {
    this.setState({
      idxTipoDisc: iIdx,
      sTestoFiltro: sTesto,
    });
  }

  render() {
    return (

      <Container style={styles.container} >
        <View style={styles.vwScroll}>
          {/* <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ alignContent: 'center' }}> */}
          <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 1 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(1, 'Inserisci il nome di un Attore')}>
            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 1 ? styles.txtButtonToolsSel : {}]}>Film</Text>
          </Button>
          <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 2 ? styles.btnToolsSel : {}, {flex: 1.4}]} onPress={this.selezionaTipo(2, 'Inserisci il nome di uno Scrittore')}>
            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 2 ? styles.txtButtonToolsSel : {}]}>Serie TV</Text>
          </Button>
          <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 3 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(3, 'Inserisci il nome di un Regista')}>
            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 3 ? styles.txtButtonToolsSel : {}]}>Libro</Text>
          </Button>
          {/* </ScrollView> */}
        </View>
        <Tabs locked={true} tabBarUnderlineStyle={{ borderBottomWidth: 2, borderColor: '#808080' }} tabContainerStyle={{ height: 80 }}>
          <Tab heading={
            <TabHeading activeTabStyle={styles.tabSel} activeTextStyle={styles.tabSel} style={styles.tabStyle}>
              {/* <Icon name='ios-people-outline' style={{fontSize: 40, textAlign: 'center', color: '#808080'}} /> */}
              <Text style={styles.tabText}>Chiedi a{"\n"}The Story</Text>
            </TabHeading>}>
            <TabChiediTS />
          </Tab>
          <Tab heading={
            <TabHeading activeTabStyle={styles.tabSel} activeTextStyle={styles.tabSel} style={styles.tabStyle}>
              {/* <Icon name='ios-globe-outline' style={{fontSize: 40, textAlign: 'center', color: '#808080'}} /> */}
              <Text style={styles.tabText}>Chiedi alla{"\n"}Community</Text></TabHeading>}>
            <TabChiediCommunity />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  vwScroll: {
    height: 40,
    flexDirection: 'row',
    borderBottomColor: '#7a42f4',
    borderBottomWidth: 1,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
  },
  btnTools: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 5,
    marginRight: 5,
    flex: 1
  },
  btnToolsSel: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: 'rgba(63,81,181, .9)',
  },
  txtButtonTools: {
    fontSize: 15,
    fontFamily: 'GoogleSans-Regular',
    color: '#cdcdcd',
    textAlign: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  txtButtonToolsSel: {
    fontSize: 15,
    fontFamily: 'GoogleSans-Bold',
    color: '#fff',
    textAlign: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  container: {
    backgroundColor: '#F5FCFF',
  },
  tabStyle: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F5F5F5',
    height: 80,
  },
  tabSel: {
    fontSize: 40,
    fontFamily: 'GoogleSans-Bold',
    color: '#808080',
  },
  tabText: {
    fontSize: 20,
    fontFamily: 'GoogleSans-Regular',
    color: '#808080',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
});
