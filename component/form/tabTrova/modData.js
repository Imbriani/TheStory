import React, { Component } from 'react';
import { StyleSheet, Switch, TextInput } from 'react-native';
import { Container, Separator, Text, List, ListItem, Body, View, Icon, Button } from "native-base";

export default class modData extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sDal: '',
            sAl: '',
        };
    }

    render() {
        return (
            <Container>
                <View style={{ flex: .6 }}>
                    <Text style={styles.Text}>Anno</Text>
                </View>
                <View style={{ flex: 4 }}>
                    <TextInput keyboardType = 'numeric' style={[styles.txtAnno, { borderBottomWidth: 1, borderColor: '#7a42f4' }]} placeholder='Da'
                        onChangeText={(sDal) => this.setState({sDal})}>
                    </TextInput>               
                    <TextInput keyboardType = 'numeric' style={[styles.txtAnno, { borderBottomWidth: 1, borderColor: '#7a42f4' }]} placeholder='Al'
                        onChangeText={(sAl) => this.setState({sAl})}>
                    </TextInput>    
                </View>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center', position: 'absolute' }}>
                        <Button rounded style={styles.btnSalva} onPress={this.props.onPress(`${this.state.sDal} - ${this.state.sAl}`, 'ppp')} >
                            <Text style={styles.txtButton} uppercase={false}>Ok</Text>
                        </Button>
                        <Button rounded style={styles.btnSalva} onPress={this.props.onAnnulla} >
                            <Text style={styles.txtButton} uppercase={false}>Annulla</Text>
                        </Button>
                    </View>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    Text: {
        fontSize: 20,
        textAlign: 'left',
        fontFamily: 'GoogleSans-Bold',
        margin: 5
    },
    btnSalva: {
        width: 100,
        height: 30,
        bottom: 10,
        alignItems: 'flex-end',
        flex: 1,
        justifyContent: 'flex-end',
        marginLeft: 5,
        marginRight: 5,
    },
    txtButton: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
        bottom: 2,
    },
    txtAnno: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        justifyContent: 'flex-start',
        margin: 5,
    },    
});
