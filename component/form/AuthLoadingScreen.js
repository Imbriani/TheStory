import React, { Component } from 'react';
import { Platform, StyleSheet, StatusBar, View, ActivityIndicator, AsyncStorage, Image } from 'react-native';

export default class AuthLoadingScreen extends React.Component {

    constructor(){
        super()
        this.loadApp()
    }

    loadApp = async () => {
        const userToken = await AsyncStorage.getItem('userToken')

        this.props.navigation.navigate(userToken ? 'App' : 'Auth')
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle='light-content' backgroundColor="#1565c0" />
                <Image source={require('../../assets/img/Logo.png')} style={{ width: 150, height: 178, marginBottom: 20 }} />
                <ActivityIndicator size="large" />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
