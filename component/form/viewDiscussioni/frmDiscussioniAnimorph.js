import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Alert, Dimensions, Image, Button } from 'react-native';
import { Container, Content, Card, CardItem, Thumbnail, Icon } from 'native-base';
import SegmentedControlTab from 'react-native-segmented-control-tab';

export default class frmDiscussioniAni extends Component {

    static navigationOptions = {
        title: 'Discussioni',
        headerTintColor: '#696969',
        headerTitleStyle: {
            fontSize: 20,
            fontFamily: 'GoogleSans-Regular',
        },
        headerRight: (
            <Button
              title="Nuova Discussione"
              onPress={() => alert('Nuova discussione')}
            />
          ),  
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
        };
    }

    handleIndexChange = (index) => {
        this.setState({
            ...this.state,
            selectedIndex: index,
        });
    }

    render() {
        return (
            <Container>
                <Content padder>
                    <View style={{ margin: 10 }}>
                        <SegmentedControlTab
                            values={['Più rilevanti', 'Più recenti']}
                            selectedIndex={this.state.selectedIndex}
                            onTabPress={this.handleIndexChange}
                            activeTabStyle={styles.activeTabStyle}
                            tabTextStyle={styles.tabTextStyle}
                        />
                    </View>

{/* Discussion 3 */}
                    <Card>
                        <CardItem>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 74, backgroundColor: '#1565c0', borderBottomWidth: 1, borderBottomColor: 'rgb(128,128,128)' }} >
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#808080', borderBottomWidth: 1, left: 1 }}>
                                <Thumbnail style={styles.tmbProfile} source={require('../../../assets/img/Gabriele.png')} />
                                <View style={{ flex: 1, flexDirection: 'column'}} button onPress={() => Alert.alert('hi')}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Discussione', {
                                        sTitolo: "Alter Ego: Memorie di un viaggiatore ultracorporeo",
                                        sIncipit: "Francia, un ragazzo, si risveglia su una roccia in mezzo al bosco. Non sa come si chiama. Ha solo una ferita profonda sul polso. Grazie all'aiuto della comunità di un piccolo paesino ai confini di Parigi riesce ad iniziare una vita nuova con un nuovo nome, Ariel. Ma presto scopre qualcosa di strano, un potere. Questo suo potere gli permetterà di lavorare ai servigi di persone influenti soprattutto durante la Rivoluzione Francese. Ma chi è veramente Ariel? ",
                                        sTrailer: "Sentii come un fremito improvviso partire dal braccio e percorrere tutto il corpo. Poi vidi una luce, tanto accecante da chiudere gli occhi. Quando li riaprii non credetti a quello che mi trovavo di fronte: il fiume dall'alto, una mano tesa verso il basso e nessuno lì sotto a tenersi aggrappato. Ero nel corpo del mio sconosciuto salvatore, mentre il mio era scomparso improvvisamente e i vestiti, che si erano svuotati di colpo, stavano cadendo nell'acqua sottostante.",
                                        sTrama: "Un ragazzo si sveglia in un bosco alle porte di Parigi senza memoria del suo passato. Tramite un fortuito incidente, scopre di possedere un inspiegabile potere in grado di farlo trasmigrare nel corpo di altre persone, smettendo di esistere e invecchiare durante la permanenza nei suoi ospiti. Il protagonista trasformerà il suo singolare dono in una sinistra professione al servizio della massoneria e dei potenti della Francia settecentesca, grazie a questa capacità la sua vita si intreccerà con quella di famosi personaggi dell’epoca. Attraverso viaggi esotici, sesso, amori dannati, amicizie altolocate e nemici potenti che tramano nell’ombra, culminando in un colpo di scena finale.",
                                        sProtagonista: "Il protagonista di Alter Ego è un ragazzo di quindici annii, che venne chiamatao Ariel Des Anges. Un personaggio misterioso, senza memoria. Ma che presto capisce chi potrebbe essere. Chiunque egli voglia. E questo potere plasmerà definitivamente la sua personalità.",
                                        sAmbientazione: "Quella di Alterego è un’avventura lunga più di un secolo che ha inizio in Francia, a Parigi, nel 1745. Un percorso che si realizza in un crescendo sempre più ritmato e che ci farà vivere in prima persona un'escalation di momenti storici, tra cui la rivoluzione francese. Alter Ego racconta uno scorcio su una delle epoche più buie della storia, con un vivo retrogusto esoterico.",
                                        sImmagine: require('../../../assets/imgStorie/Alterego.jpg'),
                                        sVoto: this.state.valoreVoto2,
                                    })}>
                                    <Text style={styles.CardTextHeaderUtente}>Gabriele Litti</Text>
                                    <Text style={styles.CardTextHeader}>Animorph</Text>
                                    </TouchableOpacity>
                                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, }}>
                                        <View style={{ marginRight: 5, width: viewportWidth - 225 }}>
                                            <View style={{ width: 120, marginBottom: 5, borderRadius: 70, borderColor: 'rgb(128,128,128)', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                <Text style={styles.txtVotoMedio}>5,4 Voto medio</Text>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1,  }}> 
                                            <TouchableOpacity style={{alignItems: 'flex-end'}}>
                                                <View style={{ width: 60, borderRadius: 70, borderColor: 'rgb(128,128,128)', backgroundColor: '#90EE90', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                    <Text style={styles.txtSegui}>Unisciti</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Text style={styles.CardTextDetail} note>Questo libro mi piace talmente tanto che lo sto pubblicizzando a destra e a manca</Text>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={styles.viewDiscussione}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../../assets/img/discussione2.png')} style={{ width: 20, height: 20 }} />
                                        <Text style={styles.txtNDiscussioni}>333</Text>
                                    </View>
                                    <TouchableOpacity>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                            <Icon name='ios-thumbs-up-outline' style={{ fontSize: 20, color: '#696969' }} />
                                            <Text style={styles.txtNDiscussioni}>51</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </CardItem>
                    </Card>

                </Content>
            </Container>
        );
    }
}

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    modal: {
        height: 115,
        flex: 1,
        flexDirection: 'column',
        borderWidth: .4,
        borderColor: 'rgb(47,79,79)',
        backgroundColor: '#FFFFFF'
    },
    modalFiltro: {
        flex: 1,
        flexDirection: 'column',
        borderWidth: .4,
        borderColor: 'rgb(47,79,79)',
        backgroundColor: '#FFFFFF',
        width: 300,
        marginTop: 5,
        marginBottom: 30,
    },
    divTouch: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 3,
        marginRight: 3,
    },
    divVoto: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 3,
        marginRight: 3,
    },
    divVotoScarso: {
        backgroundColor: '#f44336',
    },
    divVotoMediocre: {
        backgroundColor: '#ffee58',
    },
    divVotoBuono: {
        backgroundColor: '#9ccc65',
    },
    txtVotoFloat: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        justifyContent: 'center',
    },
    txtVotoFloatScarso: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        justifyContent: 'center',
        color: '#DCDCDC',
    },
    CardTextHeaderUtente: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'left',
        marginLeft: 5,
        color: '#fff',
    },    
    CardTextHeader: {
        fontSize: 13,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 5,
        color: '#fff',
        maxHeight: 32,
        minHeight: 32,
    },
    CardTextHeaderNote: {
        fontSize: 13,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 5,
        marginBottom: 2,
    },
    txtSegui: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
    },
    txtAvviaDiscussione: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        color: '#fff',
    },
    txtVotoMedio: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
        marginRight: 5,
    },
    txtVoto: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        margin: 8,
        marginTop: 17,
    },
    viewDiscussione: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        height: 40,
        borderTopWidth: 1,
        borderColor: '#808080',
        alignItems: 'center',
    },
    txtNDiscussioni: {
        fontSize: 17,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
    },
    txtAudio: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 8,
        color: '#C0C0C0',
    },
    CardTextDetail: {
        fontSize: 17,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'justify',
        marginRight: 5,
    },
    tmbProfile: {
        borderWidth: 1,
        // borderColor: '#696969',
        borderColor: '#ffffff',
        marginTop: 1.5,
        marginBottom: 5,
        marginLeft: 2,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    popOverStyle: {
        backgroundColor: '#ecf0f1',
    },
    viewIcone: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        height: 65,
        paddingLeft: 5,
        paddingTop: 5,
        borderBottomWidth: 1,
    },
    txtView: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        margin: 15,
    },
    iconFiltro: {
        fontSize: 30,
        textAlign: 'center',
    },
    txtIcon: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
    },
    txtTime: {
        fontSize: 25,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtPosizione: {
        fontSize: 25,
        color: '#FFFF00',
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeTabStyle: {
        backgroundColor: '#3F51B5'
    },
    tabTextStyle: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
    }    
});
