import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Alert, Dimensions, Button } from 'react-native';
import { Container, Content, Card, CardItem, Thumbnail, Icon, Form, Item, Label, Input } from 'native-base';

export default class frmDiscussioneAni extends Component {

    static navigationOptions = {
        title: 'Discussione',
        headerTintColor: '#696969',
        headerTitleStyle: {
            fontSize: 20,
            fontFamily: 'GoogleSans-Regular',
        },
        headerRight: (
          <Button
            title="Unisciti"
            onPress={() => alert('Ti sei unito alla discussione')}
          />
        ),
      };

    render() {
        return (
            <Container>
                <Content padder>
                    <Card transparent>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={styles.viewDiscussioneTop}>
                                    <Thumbnail style={styles.tmbProfile} source={require('../../../assets/img/Gabriele.png')} />
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                        <Text style={styles.txtNome} note>Gabriele Litti</Text>
                                        <Text style={styles.txtDettaglio} note>Questo libro mi piace talmente tanto che lo sto pubblicizzando a destra e a manca</Text>
                                    </View>
                                </View>

                                <View style={styles.viewDiscussioneBottom}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginBottom: 3 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                                            <Text style={styles.txtNDiscussioni}>04/10/2018 14:44</Text>
                                        </View>
                                        <TouchableOpacity style={{ marginRight: 10 }}>
                                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                <Icon name='ios-thumbs-up-outline' style={{ fontSize: 20, color: '#696969' }} />
                                                <Text style={styles.txtNDiscussioni}>23</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </CardItem>

                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-end' }}>
                                    <View style={styles.viewDiscussioneTopIo}>
                                        <View style={{ flex: 1, flexDirection: 'column' }}>
                                            <Text style={styles.txtNome} note>Gabriele Litti</Text>
                                            <Text style={styles.txtDettaglio} note>Questo libro mi piace talmente tanto che lo sto pubblicizzando a destra e a manca</Text>
                                        </View>
                                        <Thumbnail style={styles.tmbProfileIo} source={require('../../../assets/img/Gabriele.png')} />
                                    </View>

                                    <View style={styles.viewDiscussioneBottomIo}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginBottom: 3 }}>
                                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                                                <Text style={styles.txtNDiscussioni}>04/10/2018 14:44</Text>
                                            </View>
                                            <TouchableOpacity style={{ marginRight: 10 }}>
                                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                    <Icon name='ios-thumbs-up-outline' style={{ fontSize: 20, color: '#696969' }} />
                                                    <Text style={styles.txtNDiscussioni}>23</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </CardItem>

                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-end' }}>
                                    <View style={styles.viewDiscussioneTopIo}>
                                        <View style={{ flex: 1, flexDirection: 'column' }}>
                                            <Text style={styles.txtNome} note>Gabriele Litti</Text>
                                            <Text style={styles.txtDettaglio} note>Prova</Text>
                                        </View>
                                        <Thumbnail style={styles.tmbProfileIo} source={require('../../../assets/img/Gabriele.png')} />
                                    </View>

                                    <View style={styles.viewDiscussioneBottomIo}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginBottom: 3 }}>
                                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                                                <Text style={styles.txtNDiscussioni}>04/10/2018 14:44</Text>
                                            </View>
                                            <TouchableOpacity style={{ marginRight: 10 }}>
                                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                    <Icon name='ios-thumbs-up-outline' style={{ fontSize: 20, color: '#696969' }} />
                                                    <Text style={styles.txtNDiscussioni}>23</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </CardItem>

                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={styles.viewDiscussioneTop}>
                                    <Thumbnail style={styles.tmbProfile} source={require('../../../assets/img/Gabriele.png')} />
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                        <Text style={styles.txtNome} note>Gabriele Litti</Text>
                                        <Text style={styles.txtDettaglio} note>Questo libro mi piace talmente tanto che lo sto pubblicizzando a destra e a manca</Text>
                                    </View>
                                </View>

                                <View style={styles.viewDiscussioneBottom}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginBottom: 3 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                                            <Text style={styles.txtNDiscussioni}>04/10/2018 14:44</Text>
                                        </View>
                                        <TouchableOpacity style={{ marginRight: 10 }}>
                                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                <Icon name='ios-thumbs-up-outline' style={{ fontSize: 20, color: '#696969' }} />
                                                <Text style={styles.txtNDiscussioni}>23</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </CardItem>
                    </Card>
                </Content>
                <Item floatingLabel style={styles.viewInvioMessaggio}>
                    <Input style={styles.inputItem} placeholder="Scrivi un messaggio..." />
                    <Label style={styles.lblItem}>Testo</Label>
                    <Icon name='ios-send-outline' style={{ fontSize: 35, color: '#696969', marginBottom: 5, marginRight: 5 }} />
                </Item>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 20,
        fontFamily: 'Google Sans',
        textAlign: 'center',
        margin: 10,
    },
    viewDiscussioneTop: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        maxWidth: 250,
        backgroundColor: '#ecf0f1',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        borderBottomWidth: 1,
        borderColor: '#C0C0C0',
    },
    viewDiscussioneBottom: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        maxWidth: 250,
        backgroundColor: '#ecf0f1',
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
    },
    viewDiscussioneTopIo: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        maxWidth: 250,
        backgroundColor: '#FFEFD5',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        borderBottomWidth: 1,
        borderColor: '#C0C0C0',
    },
    viewDiscussioneBottomIo: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        maxWidth: 250,
        backgroundColor: '#FFEFD5',
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
    },
    tmbProfile: {
        borderWidth: 1,
        // borderColor: '#696969',
        borderColor: '#ffffff',
        marginTop: 2,
        marginBottom: 5,
        marginLeft: 3,
    },
    tmbProfileIo: {
        borderWidth: 1,
        // borderColor: '#696969',
        borderColor: '#ffffff',
        marginTop: 2,
        marginBottom: 5,
        marginRight: 3,
    },
    txtNome: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'justify',
        marginLeft: 10,
        maxWidth: 180,
        marginTop: 3,
    },
    txtDettaglio: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'justify',
        marginLeft: 10,
        maxWidth: 180,
        marginBottom: 5,
        marginTop: 3,
    },
    txtNDiscussioni: {
        fontSize: 12,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginRight: 5,
    },
    viewInvioMessaggio: {
        justifyContent: 'center',
        height: 60,
        backgroundColor: '#FFEFD5',
        borderWidth: 1,
    },
    inputItem: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        backgroundColor: '#fff',
        color: '#000',
        marginRight: 15,
        marginLeft: 10,
        marginTop: 3,
        marginBottom: 15,
        height: 40,
    },
    lblItem: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        marginLeft: 10,
        color: '#FFEFD5',
    },
});
