import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Alert, Dimensions, Image, Switch } from 'react-native';
import { Container, Content, Card, CardItem, Body, Right, Thumbnail, Icon, Left, Separator, Fab, Button, List, ListItem } from 'native-base';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import Modal from 'react-native-modalbox';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import FiltroScreen from './modFiltro';

export default class tabTop10 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            active: 'false',
            selectedIndexVoti: 0,
            valoreVoto1: 'Vota',
            valoreVoto2: 'Vota',
            valoreVoto3: 'Vota',
            valoreStar: 0,
        };
        this.ChiudiMod = this.ChiudiMod.bind(this);
    }

    ChiudiMod = async () => {
        this.refs.modFiltro.close()
    }

    votaStar = (iSt) => () => {
        this.setState({
            valoreStar: iSt,
        });
    }

    Voto1 = (iVoto) => () => {
        this.setState({
            valoreVoto1: iVoto,
        });
        this.refs.modVoto.close()
    }

    Voto2 = (iVoto) => () => {
        this.setState({
            valoreVoto2: iVoto,
        });
        this.refs.modVoto2.close()
    }

    Voto3 = (iVoto) => () => {
        this.setState({
            valoreVoto3: iVoto,
        });
        this.refs.modVoto3.close()
    }

    SignUp = async () => {
        await AsyncStorage.setItem('userToken', '')

        this.props.navigation.navigate('App')
    }

    handleIndexChangeVoti = (index) => {
        this.setState({
            ...this.state,
            selectedIndexVoti: index,
        });
    }
    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };

    render() {
        return (
            <Container>
                <Content padder>
                    <View style={styles.viewIcone}>
                        <Text style={styles.txtIcon}>Le 10 storie migliori storie secondo la community</Text>
                    </View>
                    <View style={styles.viewIcone}>
                        <Text style={styles.txtTime}>08:33:58 alla fine</Text>
                    </View>

                    {/* STORIA 2 */}
                    <Card>
                        <CardItem>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 70, backgroundColor: '#1565c0', borderBottomWidth: 1, borderBottomColor: 'rgb(128,128,128)' }} >
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#808080', borderBottomWidth: 1 }}>
                                <View style={{ height: 20, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={styles.txtPosizione}>1ª</Text>
                                </View>
                                <Thumbnail large style={styles.tmbProfile} source={require('../../../assets/imgStorie/Alterego.jpg')} />
                                <View style={{ flex: 1, flexDirection: 'column' }} button onPress={() => Alert.alert('hi')}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DettaglioTS', {
                                        sTitolo: "Alter Ego: Memorie di un viaggiatore ultracorporeo",
                                        sIncipit: "Francia, un ragazzo, si risveglia su una roccia in mezzo al bosco. Non sa come si chiama. Ha solo una ferita profonda sul polso. Grazie all'aiuto della comunità di un piccolo paesino ai confini di Parigi riesce ad iniziare una vita nuova con un nuovo nome, Ariel. Ma presto scopre qualcosa di strano, un potere. Questo suo potere gli permetterà di lavorare ai servigi di persone influenti soprattutto durante la Rivoluzione Francese. Ma chi è veramente Ariel? ",
                                        sTrailer: "Sentii come un fremito improvviso partire dal braccio e percorrere tutto il corpo. Poi vidi una luce, tanto accecante da chiudere gli occhi. Quando li riaprii non credetti a quello che mi trovavo di fronte: il fiume dall'alto, una mano tesa verso il basso e nessuno lì sotto a tenersi aggrappato. Ero nel corpo del mio sconosciuto salvatore, mentre il mio era scomparso improvvisamente e i vestiti, che si erano svuotati di colpo, stavano cadendo nell'acqua sottostante.",
                                        sTrama: "Un ragazzo si sveglia in un bosco alle porte di Parigi senza memoria del suo passato. Tramite un fortuito incidente, scopre di possedere un inspiegabile potere in grado di farlo trasmigrare nel corpo di altre persone, smettendo di esistere e invecchiare durante la permanenza nei suoi ospiti. Il protagonista trasformerà il suo singolare dono in una sinistra professione al servizio della massoneria e dei potenti della Francia settecentesca, grazie a questa capacità la sua vita si intreccerà con quella di famosi personaggi dell’epoca. Attraverso viaggi esotici, sesso, amori dannati, amicizie altolocate e nemici potenti che tramano nell’ombra, culminando in un colpo di scena finale.",
                                        sProtagonista: "Il protagonista di Alter Ego è un ragazzo di quindici annii, che venne chiamatao Ariel Des Anges. Un personaggio misterioso, senza memoria. Ma che presto capisce chi potrebbe essere. Chiunque egli voglia. E questo potere plasmerà definitivamente la sua personalità.",
                                        sAmbientazione: "Quella di Alterego è un’avventura lunga più di un secolo che ha inizio in Francia, a Parigi, nel 1745. Un percorso che si realizza in un crescendo sempre più ritmato e che ci farà vivere in prima persona un'escalation di momenti storici, tra cui la rivoluzione francese. Alter Ego racconta uno scorcio su una delle epoche più buie della storia, con un vivo retrogusto esoterico.",
                                        sImmagine: require('../../../assets/imgStorie/Alterego.jpg'),
                                        sVoto: this.state.valoreVoto2,
                                    })}>
                                        <Text style={styles.CardTextHeader}>Alter Ego: Memorie di un viaggiatore ultracorporeo</Text>
                                    </TouchableOpacity>
                                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 20, }}>
                                        <View style={{ marginRight: 5, width: viewportWidth - 225 }}>
                                            <View style={{ width: 120, marginBottom: 5, borderRadius: 70, borderColor: 'rgb(128,128,128)', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                <Text style={styles.txtVotoMedio}>8,3 Voto medio</Text>
                                            </View>
                                        </View>
                                        <View>
                                            <TouchableOpacity>
                                                <View style={{ width: 50, borderRadius: 70, borderColor: 'rgb(128,128,128)', backgroundColor: '#90EE90', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                    <Text style={styles.txtSegui}>Segui</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ width: viewportWidth - 125 }}>
                                    <Text style={styles.CardTextDetail} note>Francia, un ragazzo, si risveglia su una roccia in mezzo al bosco. Non sa come si chiama. Ha solo una ferita profonda sul polso. Grazie all'aiuto della comunità di un piccolo paesino ai confini di Parigi riesce ad iniziare una vita nuova con un nuovo nome, Ariel. Ma presto scopre qualcosa di strano, un potere. Questo suo potere gli permetterà di lavorare ai servigi di persone influenti soprattutto durante la Rivoluzione Francese. Ma chi è veramente Ariel? </Text>
                                </View>
                                <TouchableOpacity style={{ width: 68, alignItems: 'center', justifyContent: 'center' }} onPress={this.votaStar(2)}>
                                    <View>
                                        {
                                            this.state.valoreStar == 2 ? <Image source={require('../../../assets/img/starGialla.png')} style={{ width: 50, height: 50 }} /> : <Image source={require('../../../assets/img/star.png')} style={{ width: 50, height: 50 }} />
                                        }
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, flexDirection: 'column', marginBottom: 8 }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='md-arrow-dropright-circle' style={{ fontSize: 35, color: '#696969' }} />
                                        <View style={{ flex: 1, flexDirection: 'row', borderRadius: 70, borderWidth: 1, marginRight: 10, height: 20, alignItems: 'center' }}>
                                            <Text style={styles.txtAudio}>
                                                Trailer: 00:30
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='md-arrow-dropright-circle' style={{ fontSize: 35, color: '#696969' }} />
                                        <View style={{ flex: 1, flexDirection: 'column', borderRadius: 70, borderWidth: 1, marginRight: 10, height: 20 }}>
                                            <Text style={styles.txtAudio}>
                                                Trailer: 01:00
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ flex: 0, flexDirection: 'column' }}>
                                    <TouchableOpacity onPress={() => this.refs.modVoto2.open()}>
                                        <View style={{ borderRadius: 100, width: 65, height: 65, borderWidth: 1, backgroundColor: '#90EE90', alignItems: 'center' }}>
                                            <Text style={styles.txtVoto}>
                                                {this.state.valoreVoto2}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <Modal style={[styles.modal]} position={"center"} swipeToClose={false} ref={"modVoto2"}>
                                <TouchableOpacity onPress={() => this.refs.modVoto2.close()}>
                                    <View style={{ alignItems: 'center' }}>
                                        <Icon name='ios-arrow-dropdown-outline' style={{ fontSize: 25 }} />
                                    </View>
                                </TouchableOpacity>
                                <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto2(1)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>1</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto2(2)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>2</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto2(3)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>3</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto2(4)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>4</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoMediocre]} onPress={this.Voto2(5)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>5</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoMediocre]} onPress={this.Voto2(6)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>6</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoMediocre]} onPress={this.Voto2(7)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>7</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoBuono]} onPress={this.Voto2(8)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>8</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoBuono]} onPress={this.Voto2(9)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>9</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoBuono]} onPress={this.Voto2(10)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>10</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </Modal>
                        </CardItem>
                        <CardItem>
                            <View style={styles.viewDiscussione}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                </View>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginTop: 4 }}>
                                    <TouchableOpacity>
                                        <View style={{ width: 250, height: 30, borderColor: 'rgb(128,128,128)', backgroundColor: '#1565c0', borderRadius: 50, borderWidth: 1, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={styles.txtAvviaDiscussione}>Avvia per primo una discussione</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </CardItem>
                    </Card>

                    {/* STORIA 1 */}
                    <Card>
                        <CardItem>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 70, backgroundColor: '#1565c0', borderBottomWidth: 1, borderColor: 'rgb(128,128,128)' }} >
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#808080', borderBottomWidth: 1 }}>
                                <View style={{ height: 20, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={styles.txtPosizione}>2ª</Text>
                                </View>
                                <Thumbnail large style={styles.tmbProfile} source={require('../../../assets/imgStorie/Animorph.jpg')} />
                                <View style={{ flex: 1, flexDirection: 'column', }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DettaglioTS', {
                                        sTitolo: "Animorph",
                                        sIncipit: "Si chiamano Yeerk, gli alieni-parassiti più pericolosi della Galassia. Hanno già pianificato tutto. L’invasione del pianeta Terra è prossima. Sono sicuri di farcela, anche questa volta. Soltanto l’alleanza tra esseri umani e Andaliti potrebbe contrastarli, e un terrestre di nome Jake sta per scoprirlo. ",
                                        sTrailer: "Mi chiamo Jake. Il cognome non posso dirlo. Sarebbe troppo pericoloso. I Controller sono dovunque. Dovunque. E se sapessero come mi chiamo, potrebbero rintracciare me e i miei amici, e allora… be’, diciamo che l’idea non mi sorride. Quello che fanno a chiunque apponga loro resistenza è orribile. E neanche vi dirò dove vivo. Però vi garantisco che è un posto reale, una città reale. Magari proprio la vostra. Sto registrando questo nastro perché più gente possibile venga a conoscenza della verità. Forse, allora, la razza umana riuscirà a sopravvivere finché gli Andaliti torneranno ad aiutarci. Forse.",
                                        sTrama: "Protagonisti di Animorph sono cinque ragazzi (Jake, Rachel, Cassie, Tobias e Marco) che fortuitamente scoprono che la Terra sta subendo una silenziosa invasione aliena da parte degli Yeerk, una razza di esseri simili a lumaconi che hanno la capacità di avvolgersi intorno al cervello delle altre razze per assumere il controllo del loro corpo, trasformandole in creature chiamate Controller. I cinque vengono avvertiti del pericolo da un altro alieno, un Andalita, che dona loro una portentosa tecnologia che consente ai ragazzi di trasformarsi in qualunque animale tocchino. Il potere ha un'importante limitazione: se non si inverte la metamorfosi entro due ore, questa diventerà permanente. I cinque protagonisti, a cui si aggiungeranno altri personaggi, decidono di nominare se stessi 'Animorph'. Gli Animorph combattono gli Yeerk in incognito, senza mai rivelare il loro vero aspetto umano ai loro nemici che, per contro, si convincono di avere a che fare con dei banditi Andaliti, essendo questi ufficialmente l'unica specie in possesso del potere della metamorfosi.",
                                        sProtagonista: "Jake Berenson ha sempre dimostrato la sua maturità. Sin da ragazzino era quello che gli altri ammiravano e seguivano. Un leader. Concreto, responsabile, coraggioso e deciso. Ecco perché si è guadagnato il rispetto dei suoi compagni di battaglia, che sono portati spontaneamente ad affidarsi a lui nei momenti di maggior pericolo. Ma Jake accetta definitivamente il suo ruolo di capo degli Animorph soltanto quando scopre che suo fratello Tom è un Controller. Con l’obiettivo iniziale di liberare Tom dallo Yeerk che lo infesta Jake darà inizio alla logorante lotta contro i parassiti alieni e sarà costretto a prendere decisioni molto difficili.",
                                        sAmbientazione: "Le vicende di Animorph hanno inizio a New York, in un futuro molto prossimo. Una City che nei primi capitoli sarà proprio come quella che conosciamo, multietnica e cosmopolita. Ma la grande mela si trasformerà, in pochissimo tempo, nel primo, grande, campo di battaglia per la guerra tra yeerk ed esseri umani. E la frenesia ed il caos della metropoli lasceranno spazio a silenzio e desolazione, alternati a momenti di urla e terrore.",
                                        sImmagine: require('../../../assets/imgStorie/Animorph.jpg'),
                                        sVoto: this.state.valoreVoto1,
                                    })}>
                                        <Text style={styles.CardTextHeader}>Animorph</Text>
                                    </TouchableOpacity>

                                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 20, }}>
                                        <View style={{ marginRight: 5, width: viewportWidth - 225 }}>
                                            <View style={{ width: 120, marginBottom: 5, borderRadius: 70, borderColor: 'rgb(128,128,128)', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                <Text style={styles.txtVotoMedio}>8,5 Voto medio</Text>
                                            </View>
                                        </View>
                                        <View>
                                            <TouchableOpacity>
                                                <View style={{ width: 50, borderRadius: 70, borderColor: 'rgb(128,128,128)', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                    <Text style={styles.txtSegui}>Segui</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ width: viewportWidth - 125 }}>
                                    <Text style={styles.CardTextDetail} note>Si chiamano Yeerk, gli alieni-parassiti più pericolosi della Galassia. Hanno già pianificato tutto. L’invasione del pianeta Terra è prossima. Sono sicuri di farcela, anche questa volta. Soltanto l’alleanza tra esseri umani e Andaliti potrebbe contrastarli, e un terrestre di nome Jake sta per scoprirlo.</Text>
                                </View>
                                <TouchableOpacity style={{ width: 68, alignItems: 'center', justifyContent: 'center' }} onPress={this.votaStar(1)}>
                                    <View>
                                        {
                                            this.state.valoreStar == 1 ? <Image source={require('../../../assets/img/starGialla.png')} style={{ width: 50, height: 50 }} /> : <Image source={require('../../../assets/img/star.png')} style={{ width: 50, height: 50 }} />
                                        }
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, flexDirection: 'column', marginBottom: 8 }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='md-arrow-dropright-circle' style={{ fontSize: 35, color: '#696969' }} />
                                        <View style={{ flex: 1, flexDirection: 'row', borderRadius: 70, borderWidth: 1, marginRight: 10, height: 20, alignItems: 'center' }}>
                                            <Text style={styles.txtAudio}>
                                                Trailer: 00:30
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='md-arrow-dropright-circle' style={{ fontSize: 35, color: '#696969' }} />
                                        <View style={{ flex: 1, flexDirection: 'column', borderRadius: 70, borderWidth: 1, marginRight: 10, height: 20 }}>
                                            <Text style={styles.txtAudio}>
                                                Trailer: 01:00
                                        </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ flex: 0, flexDirection: 'column' }}>
                                    <TouchableOpacity onPress={() => this.refs.modVoto.open()}>
                                        <View style={{ borderRadius: 100, width: 65, height: 65, borderWidth: 1, backgroundColor: '#90EE90', alignItems: 'center' }}>
                                            <Text style={styles.txtVoto}>
                                                {this.state.valoreVoto1}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <Modal style={[styles.modal]} position={"center"} swipeToClose={false} ref={"modVoto"}>
                                <TouchableOpacity onPress={() => this.refs.modVoto.close()}>
                                    <View style={{ alignItems: 'center' }}>
                                        <Icon name='ios-arrow-dropdown-outline' style={{ fontSize: 25 }} />
                                    </View>
                                </TouchableOpacity>
                                <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto1(1)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>1</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto1(2)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>2</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto1(3)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>3</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto1(4)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>4</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoMediocre]} onPress={this.Voto1(5)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>5</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoMediocre]} onPress={this.Voto1(6)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>6</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoMediocre]} onPress={this.Voto1(7)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>7</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoBuono]} onPress={this.Voto1(8)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>8</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoBuono]} onPress={this.Voto1(9)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>9</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoBuono]} onPress={this.Voto1(10)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>10</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </Modal>
                        </CardItem>
                        <CardItem>
                            <View style={styles.viewDiscussione}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../../assets/img/discussione2.png')} style={{ width: 35, height: 35 }} />
                                        <Text style={styles.txtNDiscussioni}>2.319</Text>
                                    </View>
                                    <TouchableOpacity>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginRight: 5 }}>
                                            <Icon name='ios-share-outline' style={{ fontSize: 35, color: '#696969' }} />
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Discussione')}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                            <Icon name='ios-chatbubbles-outline' style={{ fontSize: 35, color: '#696969' }} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </CardItem>
                    </Card>

                    {/* Storia 3 */}
                    <Card>
                        <CardItem>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 65, backgroundColor: '#1565c0', borderBottomWidth: 1, borderBottomColor: 'rgb(128,128,128)' }} >
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#808080', borderBottomWidth: 1 }}>
                                <View style={{ height: 20, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={styles.txtPosizione}>3ª</Text>
                                </View>                            
                                <Thumbnail large style={styles.tmbProfile} source={require('../../../assets/imgStorie/TiPregoLasciatiOdiare.png')} />
                                <View style={{ flex: 1, flexDirection: 'column' }} button onPress={() => Alert.alert('hi')}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DettaglioTS', {
                                        sTitolo: "Ti prego lasciati odiare",
                                        sIncipit: "Siamo entrambi fermi davanti a un ascensore che proprio non ne vuole sapere di arrivare. Tanta tecnologia per poi trovarsi a questo punto: non poter neanche evitare quel collega che non avresti mai voluto incontrare. Mi chiedo, 'non hanno ancora inventato qualche app che eviti figure di merda come quella che ho appena fatto?' ",
                                        sTrailer: "Ce la posso fare, ce la posso fare, ce la devo fare! Ma poi commetto un errore: guardo l’orologio. Oddio, non ce la posso fare… Sto correndo come una pazza per le strade di Londra perché per la prima volta, in quasi nove anni di onorata carriera, sono in clamoroso ritardo. Io, dipendente perfetta e capo team della migliore squadra di cervelli di consulenza fiscale di tutta la banca, sono fuori tempo massimo nel giorno di una presentazione fondamentale. Sono in ritardo di due ore sulla tabella di marcia! «Che scenetta divertente. Sono su Candid Camera?», domanda perfida una profonda voce maschile alle mie spalle. È così che è cominciato tutto quanto…",
                                        sTrama: "Il romanzo ruota attorno alle vicende di Jennifer e Ian, due avvocati d'affari di una banca con sede a Londra. Il rapporto professionale tra i due è contrastato, fatto di rivalità e invidie reciproche. I due però si ritrovano a lavorare fianco a fianco allo stesso progetto, arrivando anche a frequentarsi fuori dagli orari di lavoro; vengono persino paparazzati insieme da un quotidiano inglese, che pubblica le loro foto nella sezione gossip. Jennifer è evidentemente furiosa, Ian al contrario è divertito, tanto che Ian propone alla collega di fingersi la sua fidanzata, garantendole piena libertà d'azione nel progetto lavorativo. Lei accetta e, pur restia, viene coinvolta nel mondo aristocratico britannico di cui Ian fa parte, scoprendo man mano lati sempre più nuovi del suo apparentemente scorbutico collega. Ben presto, i due si ritrovano a condividere più di un semplice rapporto professionale che Jennifer non è inizialmente in grado di ammettere.",
                                        sProtagonista: "Jennifer Percy: avvocato fiscalista, è una donna di trentatré anni orgogliosa, intelligente, ostinata, testarda, competitiva e ironica, consapevole delle proprie capacità e dei propri difetti. Dopo la terza relazione seria naufragata, si ripromette di trovare un uomo veramente adatto a lei. Ian St. John: conte di Langley e futuro Duca di Remington, di due anni più giovane di lei, è un economista geniale e brillante, arrogante e incredibilmente bello, odioso tanto quanto scaltro. È però anche un ragazzo sensibile e che, al momento opportuno, ha meno difficoltà di Jenny a riconoscere ed ammettere i propri sentimenti.",
                                        sAmbientazione: "Ambientato principalmente nella Londra degli affari. Anche se la narrazione si sposta, in alcuni momenti, nella periferia della città, nella casa dei genitori di lei, o nelle tenute scozzesi della famiglia di lui. Prevale la descrizione degli interni ricchi e maestosi delle case nobiliari in contrasto con il piccolo appartamento in cui la protagonista vive con le amiche.",
                                        sImmagine: require('../../../assets/imgStorie/TiPregoLasciatiOdiare.png'),
                                        sVoto: this.state.valoreVoto3,
                                    })}>
                                        <Text style={styles.CardTextHeader}>Ti prego lasciati odiare</Text>
                                    </TouchableOpacity>
                                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 20, }}>
                                        <View style={{ marginRight: 5, width: viewportWidth - 225 }}>
                                            <View style={{ width: 120, marginBottom: 5, borderRadius: 70, borderColor: 'rgb(128,128,128)', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                <Text style={styles.txtVotoMedio}>7,1 Voto medio</Text>
                                            </View>
                                        </View>
                                        <View>
                                            <TouchableOpacity>
                                                <View style={{ width: 50, borderRadius: 70, borderColor: 'rgb(128,128,128)', backgroundColor: '#90EE90', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                    <Text style={styles.txtSegui}>Segui</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ width: viewportWidth - 125 }}>
                                    <Text style={styles.CardTextDetail} note>Siamo entrambi fermi davanti a un ascensore che proprio non ne vuole sapere di arrivare. Tanta tecnologia per poi trovarsi a questo punto: non poter neanche evitare quel collega che non avresti mai voluto incontrare. Mi chiedo, “non hanno ancora inventato qualche app che eviti figure di merda come quella che ho appena fatto?”.</Text>
                                </View>
                                <TouchableOpacity style={{ width: 68, alignItems: 'center', justifyContent: 'center' }} onPress={this.votaStar(3)}>
                                    <View>
                                        {
                                            this.state.valoreStar == 3 ? <Image source={require('../../../assets/img/starGialla.png')} style={{ width: 50, height: 50 }} /> : <Image source={require('../../../assets/img/star.png')} style={{ width: 50, height: 50 }} />
                                        }
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, flexDirection: 'column', marginBottom: 8 }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='md-arrow-dropright-circle' style={{ fontSize: 35, color: '#696969' }} />
                                        <View style={{ flex: 1, flexDirection: 'row', borderRadius: 70, borderWidth: 1, marginRight: 10, height: 20, alignItems: 'center' }}>
                                            <Text style={styles.txtAudio}>
                                                Trailer: 00:30
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='md-arrow-dropright-circle' style={{ fontSize: 35, color: '#696969' }} />
                                        <View style={{ flex: 1, flexDirection: 'column', borderRadius: 70, borderWidth: 1, marginRight: 10, height: 20 }}>
                                            <Text style={styles.txtAudio}>
                                                Trailer: 01:00
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ flex: 0, flexDirection: 'column' }}>
                                    <TouchableOpacity onPress={() => this.refs.modVoto3.open()}>
                                        <View style={{ borderRadius: 100, width: 65, height: 65, borderWidth: 1, backgroundColor: '#90EE90', alignItems: 'center' }}>
                                            <Text style={styles.txtVoto}>
                                                {this.state.valoreVoto3}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <Modal style={[styles.modal]} position={"center"} swipeToClose={false} ref={"modVoto3"}>
                                <TouchableOpacity onPress={() => this.refs.modVoto2.close()}>
                                    <View style={{ alignItems: 'center' }}>
                                        <Icon name='ios-arrow-dropdown-outline' style={{ fontSize: 25 }} />
                                    </View>
                                </TouchableOpacity>
                                <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto3(1)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>1</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto3(2)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>2</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto3(3)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>3</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoScarso]} onPress={this.Voto3(4)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>4</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoMediocre]} onPress={this.Voto3(5)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>5</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoMediocre]} onPress={this.Voto3(6)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>6</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoMediocre]} onPress={this.Voto3(7)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>7</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoBuono]} onPress={this.Voto3(8)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>8</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoBuono]} onPress={this.Voto3(9)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>9</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.divVoto, styles.divVotoBuono]} onPress={this.Voto3(10)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>10</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </Modal>
                        </CardItem>
                        <CardItem>
                            <View style={styles.viewDiscussione}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../../assets/img/discussione2.png')} style={{ width: 35, height: 35 }} />
                                        {/*<Icon name='ios-people-outline' style={{ fontSize: 35, color: '#696969' }} />*/}
                                        <Text style={styles.txtNDiscussioni}>25</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Discussione')}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginRight: 5 }}>
                                            <Icon name='ios-share-outline' style={{ fontSize: 35, color: '#696969' }} />
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Discussione')}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                            <Icon name='ios-chatbubbles-outline' style={{ fontSize: 35, color: '#696969' }} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </CardItem>
                    </Card>
                </Content>
                <Modal style={[styles.modalFiltro]} position={"center"} swipeToClose={false} ref={"modFiltro"}>
                    <FiltroScreen onPress={this.ChiudiMod.bind(this)} />
                </Modal>
            </Container>
        );
    }
}

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    modal: {
        height: 115,
        flex: 1,
        flexDirection: 'column',
        borderWidth: .4,
        borderColor: 'rgb(47,79,79)',
        backgroundColor: '#FFFFFF'
    },
    modalFiltro: {
        flex: 1,
        flexDirection: 'column',
        borderWidth: .4,
        borderColor: 'rgb(47,79,79)',
        backgroundColor: '#FFFFFF',
        width: 300,
        marginTop: 5,
        marginBottom: 30,
    },
    divTouch: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 3,
        marginRight: 3,
    },
    divVoto: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 3,
        marginRight: 3,
    },
    divVotoScarso: {
        backgroundColor: '#f44336',
    },
    divVotoMediocre: {
        backgroundColor: '#ffee58',
    },
    divVotoBuono: {
        backgroundColor: '#9ccc65',
    },
    txtVotoFloat: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        justifyContent: 'center',
    },
    txtVotoFloatScarso: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        justifyContent: 'center',
        color: '#DCDCDC',
    },
    CardTextHeader: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'left',
        marginLeft: 5,
        color: '#fff',
        maxHeight: 50,
        minHeight: 50,
    },
    CardTextHeaderNote: {
        fontSize: 13,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 5,
        marginBottom: 2,
    },
    txtSegui: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
    },
    txtAvviaDiscussione: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        color: '#fff',
    },
    txtVotoMedio: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
        marginRight: 5,
    },
    txtVoto: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        margin: 8,
        marginTop: 17,
    },
    viewDiscussione: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        height: 40,
        borderTopWidth: 1,
        borderColor: '#808080',
        alignItems: 'center',
    },
    txtNDiscussioni: {
        fontSize: 17,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
    },
    txtAudio: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 8,
        color: '#C0C0C0',
    },
    CardTextDetail: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'justify',
        marginRight: 5,
    },

    tmbProfile: {
        borderWidth: 1,
        // borderColor: '#696969',
        borderColor: '#ffffff',
        marginTop: 1.5,
        marginBottom: 5,
        marginLeft: 5,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    popOverStyle: {
        backgroundColor: '#ecf0f1',
    },
    viewIcone: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        height: 65,
        paddingLeft: 5,
        paddingTop: 5,
        borderBottomWidth: 1,
    },
    txtView: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        margin: 15,
    },
    iconFiltro: {
        fontSize: 30,
        textAlign: 'center',
    },
    txtIcon: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
    },
    txtTime: {
        fontSize: 25,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    viewTimer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 65,
        paddingLeft: 5,
        borderBottomWidth: 1,
    },
    txtPosizione: {
        fontSize: 25,
        color: '#FFFF00',
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
