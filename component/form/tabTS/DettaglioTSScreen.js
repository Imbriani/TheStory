import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Alert, Dimensions, Image } from 'react-native';
import { Container, Content, Card, CardItem, Body, Right, Thumbnail, Icon, Left, Separator, Fab, Button } from 'native-base';
import Modal from 'react-native-modalbox';
import DiscussioniScreen from '../viewDiscussioni/frmDiscussioniAnimorph';

export default class tabHome extends Component {
    
    componentWillMount() {
        this.setState({ valoreVoto: this.props.navigation.getParam('sVoto') });
    }

    constructor(props) {
        super(props);
        this.state = {
            // valoreVoto: 'Vota'
        };
    }

    Voto = (iVoto) => () => {
        this.setState({
            valoreVoto: iVoto,            
        });
        this.refs.modVoto.close()
    }

    render() {
        return (
            <Container>
                <Content padder>
                    <Card>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row', position: 'absolute', top: 0, left: 0, right: 0, height: 110, backgroundColor: '#1565c0', borderBottomWidth: 1, borderColor: 'rgb(128,128,128)' }} >
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, flexDirection: 'column', }}>
                                    <Text style={styles.CardTextHeader}>{this.props.navigation.getParam('sTitolo')}</Text>

                                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 25, }}>
                                        <View>
                                            <View style={{ width: 120, marginBottom: 5, borderRadius: 70, borderColor: '#fff', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                <Text style={styles.txtVotoMedio}>8,5 Voto medio</Text>
                                            </View>
                                        </View>
                                        <View style={{ marginLeft: viewportWidth - 232 }}>
                                            <TouchableOpacity>
                                                <View style={{ width: 50, borderRadius: 70, borderColor: '#fff', borderRadius: 50, borderWidth: 1, alignItems: 'center' }}>
                                                    <Text style={styles.txtSegui}>Segui</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 3}}>
                                        <View style={{ flex: 1, marginRight: 3, alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={styles.viewTipo}>
                                                <Text style={styles.textTipo}>Avventure</Text>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={styles.viewTipo}>
                                                <Text style={styles.textTipo}>Fantasy</Text>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1, marginLeft: 3, alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={styles.viewTipo}>
                                                <Text style={styles.textTipo}>Crime</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <Thumbnail large style={styles.tmbProfile} source={this.props.navigation.getParam('sImmagine')} />
                            <Icon name='md-close-circle' style={styles.iconExit} onPress={() => this.props.navigation.goBack()}/>
                        </CardItem>

                        {/* INCIPIT */}
                        <Separator bordered style={{ marginTop: 3 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'GoogleSans-Bold', textAlign: 'left' }}>Incipit</Text>
                        </Separator>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View>
                                    <Text style={styles.CardTextDetail} note>{this.props.navigation.getParam('sIncipit')}</Text>
                                </View>
                            </View>
                        </CardItem>

                        {/* TRAILER */}
                        <Separator bordered style={{ marginTop: 3 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'GoogleSans-Bold', textAlign: 'left' }}>Trailer</Text>
                        </Separator>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View>
                                    <Text style={styles.CardTextDetail} note>{this.props.navigation.getParam('sTrailer')}</Text>
                                </View>
                            </View>
                        </CardItem>                        
                        

                        {/* TRAMA */}
                        <Separator bordered style={{ marginTop: 3 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'GoogleSans-Bold', textAlign: 'left' }}>Trama</Text>
                        </Separator>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View>
                                    <Text style={styles.CardTextDetail} note>{this.props.navigation.getParam('sTrama')}</Text>
                                </View>
                            </View>
                        </CardItem>  

                        {/* PROTAGONISTA */}
                        <Separator bordered style={{ marginTop: 3 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'GoogleSans-Bold', textAlign: 'left' }}>Protagonista</Text>
                        </Separator>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View>
                                    <Text style={styles.CardTextDetail} note>{this.props.navigation.getParam('sProtagonista')}</Text>
                                </View>
                            </View>
                        </CardItem>

                        {/* AMBIENTAZIONE */}
                        <Separator bordered style={{ marginTop: 3 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'GoogleSans-Bold', textAlign: 'left' }}>Ambientazione</Text>
                        </Separator>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View>
                                    <Text style={styles.CardTextDetail} note>{this.props.navigation.getParam('sAmbientazione')}</Text>
                                </View>
                            </View>
                        </CardItem>

                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, flexDirection: 'column', marginBottom: 8 }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='md-arrow-dropright-circle' style={{ fontSize: 35, color: '#696969' }} />
                                        <View style={{ flex: 1, flexDirection: 'row', borderRadius: 70, borderWidth: 1, marginRight: 10, height: 20, alignItems: 'center' }}>
                                            <Text style={styles.txtAudio}>
                                                Trailer: 00:30
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='md-arrow-dropright-circle' style={{ fontSize: 35, color: '#696969' }} />
                                        <View style={{ flex: 1, flexDirection: 'column', borderRadius: 70, borderWidth: 1, marginRight: 10, height: 20 }}>
                                            <Text style={styles.txtAudio}>
                                                Trailer: 01:00
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => this.refs.modVoto.open()}>
                                        <View style={{ borderRadius: 100, width: 65, height: 65, borderWidth: 1, backgroundColor: '#90EE90', alignItems: 'center' }}>
                                            <Text style={styles.txtVoto}>
                                                {this.state.valoreVoto}
                                        </Text>
                                        </View>
                                    </TouchableOpacity>                                    
                                </View>
                                <View style={{ flex: 1, width: 68, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={require('../../../assets/img/star.png')} style={{ width: 50, height: 50 }} />
                                </View>
                            </View>

                                                        <Modal style={[styles.modal]} position={"center"} swipeToClose={false} ref={"modVoto"}>
                                <TouchableOpacity onPress={() => this.refs.modVoto.close()}>
                                    <View style={{ alignItems: 'center' }}>
                                        <Icon name='ios-arrow-dropdown-outline' style={{ fontSize: 25 }} />
                                    </View>
                                </TouchableOpacity>
                                <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                                    <TouchableOpacity style={styles.divVotoScarso} onPress={this.Voto(1)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>1</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.divVotoScarso} onPress={this.Voto(2)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>2</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.divVotoScarso} onPress={this.Voto(3)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>3</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.divVotoScarso} onPress={this.Voto(4)}>
                                        <View>
                                            <Text style={styles.txtVotoFloatScarso}>4</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.divVotoMediocre} onPress={this.Voto(5)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>5</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.divVotoMediocre} onPress={this.Voto(6)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>6</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.divVotoMediocre} onPress={this.Voto(7)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>7</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.divVotoBuono} onPress={this.Voto(8)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>8</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.divVotoBuono} onPress={this.Voto(9)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>9</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.divVotoBuono} onPress={this.Voto(10)}>
                                        <View>
                                            <Text style={styles.txtVotoFloat}>10</Text>
                                        </View>
                                    </TouchableOpacity> 
                                </View>
                            </Modal>
                        </CardItem>
                    </Card>
                    
                    <Separator bordered style={{ marginTop: 10 }}>
                        <Text style={{ fontSize: 18, fontFamily: 'GoogleSans-Bold', textAlign: 'center' }}>Discussioni</Text>
                    </Separator>

                    <DiscussioniScreen/>
                </Content >
            </Container >
        );
    }
}

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    iconExit: {
        position: 'absolute',
        fontSize: 35,
        right: 5,
        top: 7,
        color: '#ffffff',
        flex: 1,
        alignItems: 'flex-end',
    },
    modal: {
        height: 115,
        flex: 1,
        flexDirection: 'column',
        borderWidth: .4,
        borderColor: 'rgb(47,79,79)',
        backgroundColor: '#FFFFFF'
    },
    divVotoScarso: {
        backgroundColor: '#f44336',
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 3,
        marginRight: 3,
    },
    divVotoMediocre: {
        backgroundColor: '#ffee58',
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 3,
        marginRight: 3,
    },
    divVotoBuono: {
        backgroundColor: '#9ccc65',
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 3,
        marginRight: 3,
    },
    txtVotoFloat: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        justifyContent: 'center',
    },
    txtVotoFloatScarso: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        justifyContent: 'center',
        color: '#DCDCDC',
    },    
    CardTextHeader: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'left',
        marginLeft: 70,
        color: '#fff',
        maxHeight: 50,
        minHeight: 50,
        maxWidth: viewportWidth - 150,
    },
    CardTextHeaderNote: {
        fontSize: 13,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 5,
        marginBottom: 2,
    },
    txtSegui: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        color: '#fff'
    },
    txtAvviaDiscussione: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        color: '#fff',
    },
    txtVotoMedio: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
        marginRight: 5,
        color: '#fff',
    },
    txtVoto: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        margin: 8,
        marginTop: 17,
    },
    viewTipo: {

        marginTop: 3,
        borderRadius: 70,
        borderColor: 'rgb(128,128,128)',
        borderRadius: 50,
        borderWidth: 1,
        alignItems: 'center',
        width: '100%'
    },
    textTipo: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
        marginRight: 5,
    },
    viewDiscussione: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        height: 40,
        borderTopWidth: 1,
        borderColor: '#808080',
        alignItems: 'center',
    },
    txtNDiscussioni: {
        fontSize: 17,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
    },
    txtAudio: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 8,
        color: '#C0C0C0',
    },
    CardTextDetail: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'justify',
        marginRight: 5,
    },
    tmbProfile: {
        borderWidth: 1,
        borderColor: '#fff',
        top: 3,
        left: 3,
        // marginTop: 1,
        // marginBottom: 5,
        position: 'absolute',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    popOverStyle: {
        backgroundColor: '#ecf0f1',
    },
    activeTabStyle: {
        backgroundColor: '#3F51B5'
    },
    viewIcone: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        height: 100,
        paddingLeft: 5,
        paddingTop: 5,
        borderBottomWidth: 1,
    },
    txtView: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        margin: 15,
    },
    iconFiltro: {
        fontSize: 30,
        textAlign: 'center',
    },
    txtIcon: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
    },
});
