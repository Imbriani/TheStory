import React from 'react';
import TSScreen from './TSScreen';
import DettaglioTSScreen from './DettaglioTSScreen';
import Top10Screen from './TopTenScreen';
import DiscussioneScreen from '../viewDiscussioni/frmDiscussione';
import DiscussioniScreen from '../viewDiscussioni/frmDiscussioni';
import DiscussioneAniScreen from '../viewDiscussioni/frmDiscussioneAnimorph';
import DiscussioniAniScreen from '../viewDiscussioni/frmDiscussioniAnimorph';
import NuovaDiscussioneScreen from '../frmNuovaDiscussione';

import { createStackNavigator } from 'react-navigation';

export default createStackNavigator({
    TS: {
        screen: TSScreen,
        header: null,
        navigationOptions: {
            header: null
        }     
      },
      Top10: {
        screen: Top10Screen,  
        header: "Top 10",           
      },         
      DettaglioTS: {
        screen: DettaglioTSScreen,  
        header: null,
        navigationOptions: {
            header: null
        }            
      },      
      Discussione: {
        screen: DiscussioneScreen,     
      },
      Discussioni: {
        screen: DiscussioniScreen,     
      },     
      DiscussioneAni: {
        screen: DiscussioneAniScreen,     
      },
      DiscussioniAni: {
        screen: DiscussioniAniScreen,     
      },   
      NuovaDiscussione: {
        screen: NuovaDiscussioneScreen,     
      },                 
})