import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Switch } from 'react-native';
import { Container, Separator, Text, List, ListItem, Body, View, Icon, Button } from "native-base";

export default class modFiltro extends Component {

    state = {
        switchFFA: true,
        switchLCD: true,
        switchCGA: true,
        switchCasuale: false,
    }

    toggleSwitchFFA = () => this.setState(state => ({
        switchFFA: !state.switchFFA
    }));

    toggleSwitchLCD = () => this.setState(state => ({
        switchLCD: !state.switchLCD
    }));

    toggleSwitchCGA = () => this.setState(state => ({
        switchCGA: !state.switchCGA
    }));

    toggleSwitchCasuale = () => this.setState(state => ({
        switchCasuale: !state.switchCasuale
    }));


    render() {
        return (
            <Container>
                {/* <View style={{ height: 40 }}>
                    <TouchableOpacity  onPress={this.props.onPress} style={styles.iconExit} >
                        <Icon name='md-close-circle'/>
                    </TouchableOpacity>                    
                </View> */}

                {/* SCELTE */}
                <List>
                    <ListItem style={{ height: 67 }}>
                        <Body>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <Text style={styles.Text}>FFA</Text>
                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 3 }}>
                                    <Text style={styles.txtDescrizione}>Fantasy</Text>
                                    <Text style={styles.txtDescrizione}>Fantascienza</Text>
                                    <Text style={styles.txtDescrizione}>Avventura</Text>
                                </View>
                            </View>
                        </Body>
                        <Switch value={this.state.switchFFA} onValueChange={this.toggleSwitchFFA} />
                    </ListItem>
                    <ListItem style={{ height: 67 }}>
                        <Body>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <Text style={styles.Text}>LCD</Text>
                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 3 }}>
                                    <Text style={styles.txtDescrizione}>Lovestory</Text>
                                    <Text style={styles.txtDescrizione}>Comedy</Text>
                                    <Text style={styles.txtDescrizione}>Drama</Text>
                                </View>
                            </View>
                        </Body>
                        <Switch value={this.state.switchLCD} onValueChange={this.toggleSwitchLCD} />
                    </ListItem>
                    <ListItem style={{ height: 67 }}>
                        <Body>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <Text style={styles.Text}>CGA</Text>
                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 3 }}>
                                    <Text style={styles.txtDescrizione}>Crime</Text>
                                    <Text style={styles.txtDescrizione}>Giallo</Text>
                                    <Text style={styles.txtDescrizione}>Azione</Text>
                                </View>
                            </View>
                        </Body>
                        <Switch value={this.state.switchCGA} onValueChange={this.toggleSwitchCGA} />
                    </ListItem>
                </List>

                {/* CASUALE */}
                <List style={{ backgroundColor: '#E8E8E8' }}>
                    <ListItem style={{ height: 50 }}>
                        <Body>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                <Text style={styles.Text}>Ordine Casuale</Text>
                            </View>
                        </Body>
                        <Switch value={this.state.switchCasuale} onValueChange={this.toggleSwitchCasuale} />
                    </ListItem>
                </List>

                {/* ORDINE */}
                {
                    this.state.switchCasuale === false ?
                        <List>
                            {
                                this.state.switchFFA === true ?
                                    <ListItem style={{ height: 40 }}>
                                        <Icon name="ios-menu-outline" style={styles.iconText} />
                                        <Body>
                                            <Text style={styles.Text}>FFA</Text>
                                        </Body>
                                    </ListItem>
                                    : null
                            }
                            {
                                this.state.switchLCD === true ?
                                    <ListItem style={{ height: 40 }}>
                                        <Icon name="ios-menu-outline" style={styles.iconText} />
                                        <Body>
                                            <Text style={styles.Text}>LCD</Text>
                                        </Body>
                                    </ListItem>
                                    : null
                            }
                            {
                                this.state.switchCGA === true ?
                                    <ListItem style={{ height: 40 }}>
                                        <Icon name="ios-menu-outline" style={styles.iconText} />
                                        <Body>
                                            <Text style={styles.Text}>CGA</Text>
                                        </Body>
                                    </ListItem>
                                    : null
                            }
                        </List>
                        : null
                }
                {/* <View style={{ height: '30%'}}>

                </View> */}
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center', position: 'absolute' }}>
                    <Button rounded style={styles.btnSalva}  onPress={this.props.onPress} >
                        <Text style={styles.txtButton} uppercase={false}>Salva</Text>
                    </Button>
                    <Button rounded style={styles.btnSalva}  onPress={this.props.onPress} >
                        <Text style={styles.txtButton} uppercase={false}>Annulla</Text>
                    </Button>                    
                    </View>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    Text: {
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'GoogleSans-Bold',
    },
    txtDescrizione: {
        fontSize: 13,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'GoogleSans-Regular',
        marginRight: 7,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    iconText: {
        fontSize: 30,
        color: '#808080'
    },
    btnSalva: {
        width: 100,
        height: 30,
        bottom: 10,
        alignItems: 'flex-end',
        flex: 1,
        justifyContent: 'flex-end',
        marginLeft: 5,
        marginRight: 5,
    },
    txtButton: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
        bottom: 2,
    },
    iconExit: {
        position: 'absolute',
        fontSize: 25,
        right: 7,
        top: 7,
        color: '#808080',
        flex: 1,
        alignItems: 'flex-end',
    }
});
