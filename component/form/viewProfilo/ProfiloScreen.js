import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, SafeAreaView } from 'react-native';
import { Container, Content, Card, CardItem, Body, Right, Thumbnail, Icon, Left, Separator, Fab, Button } from 'native-base';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../../carousel/SliderEntry.style';
import SliderEntry from '../../carousel/SliderEntry';
import { connect } from 'react-redux';
import { editProfilo } from '../../redux/action'

const SLIDER_1_FIRST_ITEM = 1;

class ProfiloScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
        };
    }

    _renderItem({ item, index }) {
        return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
    }
    _renderItemWithParallax({ item, index }, parallaxProps) {
        return (
            <SliderEntry
                data={item}
                even={(index + 1) % 2 === 0}
                parallax={true}
                parallaxProps={parallaxProps}
            />
        );
    }

    momentumExample(number, title) {
        return (
            <View style={styles.exampleContainer}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.title}>{title}</Text>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: .8, marginRight: 5 }} onPress={() => this.props.navigation.navigate('ModificaTopTen')}>
                        <Icon name='ios-create-outline' style={styles.iconExit} />
                    </TouchableOpacity>
                </View>
                <Carousel
                    data={ENTRIES1} losc
                    renderItem={this._renderItem}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    inactiveSlideScale={0.95}
                    inactiveSlideOpacity={1}
                    enableMomentum={true}
                    activeSlideAlignment={'start'}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    activeAnimationType={'spring'}
                    activeAnimationOptions={{
                        friction: 4,
                        tension: 40
                    }}
                />
            </View>
        );
    }

    momentumAttori(number, title, ) {
        return (
            <View style={styles.exampleContainer}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.title}>{title}</Text>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: .8, marginRight: 5 }} onPress={() => this.props.navigation.navigate('ModificaTopTen')}>
                        <Icon name='ios-create-outline' style={styles.iconExit} />
                    </TouchableOpacity>
                </View>
                <Carousel
                    data={ATTORI} losc
                    renderItem={this._renderItem}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    inactiveSlideScale={0.95}
                    inactiveSlideOpacity={1}
                    enableMomentum={true}
                    activeSlideAlignment={'start'}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    activeAnimationType={'spring'}
                    activeAnimationOptions={{
                        friction: 4,
                        tension: 40
                    }}
                />
            </View>
        );
    }

    momentumCronologia(number, title, ) {
        return (
            <View style={styles.exampleContainer}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.title}>{title}</Text>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: .8, marginRight: 5 }} onPress={() => this.props.navigation.navigate('ModificaTopTen')}>
                        <Icon name='ios-create-outline' style={styles.iconExit} />
                    </TouchableOpacity>
                </View>
                <Carousel
                    data={CRONOLOGIA} losc
                    renderItem={this._renderItem}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    inactiveSlideScale={0.95}
                    inactiveSlideOpacity={1}
                    enableMomentum={true}
                    activeSlideAlignment={'start'}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    activeAnimationType={'spring'}
                    activeAnimationOptions={{
                        friction: 4,
                        tension: 40
                    }}
                />
            </View>
        );
    }

    getInteresse() {
        switch (this.props.profilo.iInteresse) {
            case 1:
                return <Icon name='ios-videocam-outline' style={styles.iconTitolo} />;
                break;
            case 2:
                return <Icon name='ios-bookmarks-outline' style={styles.iconTitolo} />;
                break;
            case 3:
                return <Icon name='ios-infinite-outline' style={styles.iconTitolo} />;
                break;
        };
        // console.log(this.state.iInteresse)
    }

    SalvaModifica = (data) => {
        const profilo = {
            sPathImmagine: data.Avatar,
            sNome: data.Nome,
            sCognome: data.Cognome,
            sMotto: data.Motto,
            iInteresse: data.Interesse,
            sInteresse: data.InteresseTxt,
        }
        this.props.onInputChange(profilo);
    };

    render() {

        console.log(this.props.profilo);
        const example2 = this.momentumExample(2, 'Top 10 Film');
        const example3 = this.momentumAttori(3, 'Top 10 Attori');

        const example4 = this.momentumCronologia(4, 'Film Settembre');
        const example5 = this.momentumExample(5, 'Libri Settembre');

        return (
            <SafeAreaView style={styles.safeArea}>
                <Container>
                    <Content>
                        <View style={styles.container}>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 75, backgroundColor: '#1565c0', borderBottomWidth: 1, borderBottomColor: 'rgb(128,128,128)' }} >
                            </View>
                            <View style={{ flex: 1, flexDirection: "column" }}>
                                <View style={styles.viewPicture}>
                                    {this.props.profilo.sPathImmagine === null ?
                                        <Thumbnail large style={styles.tmbProfile} source={require('../../../assets/img/user.png')} />
                                        :
                                        <Thumbnail large style={styles.tmbProfile} source={this.props.profilo.sPathImmagine} />
                                    }
                                </View>

                                <View style={styles.txtUtente}>
                                    <Text style={styles.nome}>{this.props.profilo.sNome}</Text>
                                    <Text style={styles.cognome}>{this.props.profilo.sCognome}</Text>
                                    {/*<Text style={styles.username}>@dondino</Text>*/}
                                </View>
                            </View>

                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('ModificaProfilo', {
                                    Nome: this.props.profilo.sNome,
                                    Cognome: this.props.profilo.sCognome,
                                    PathImmagine: this.props.profilo.sPathImmagine,
                                    Motto: this.props.profilo.sMotto,
                                    iInteresse: this.props.profilo.iInteresse,
                                    sInteresse: this.props.profilo.sInteresse,
                                    onSalva: this.SalvaModifica
                                })}
                                style={{ alignItems: 'center', position: 'absolute', bottom: 10, right: 10, height: 25, width: 120, borderRadius: 90, borderWidth: 1, borderColor: '#000000' }}>
                                <View >
                                    <Text stye={styles.txtBtnModifica}>Modifica Profilo</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={styles.viewInteressi}>
                                {this.getInteresse()}
                                <Text style={styles.txtIconCateg}>{this.props.profilo.sInteresse}</Text>
                            </View>

                            <View style={styles.viewStar}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('DettaglioTS', {
                                    sTitolo: "Animorph",
                                    sIncipit: "Si chiamano Yeerk, gli alieni-parassiti più pericolosi della Galassia. Hanno già pianificato tutto. L’invasione del pianeta Terra è prossima. Sono sicuri di farcela, anche questa volta. Soltanto l’alleanza tra esseri umani e Andaliti potrebbe contrastarli, e un terrestre di nome Jake sta per scoprirlo. ",
                                    sTrailer: "Mi chiamo Jake. Il cognome non posso dirlo. Sarebbe troppo pericoloso. I Controller sono dovunque. Dovunque. E se sapessero come mi chiamo, potrebbero rintracciare me e i miei amici, e allora… be’, diciamo che l’idea non mi sorride. Quello che fanno a chiunque apponga loro resistenza è orribile. E neanche vi dirò dove vivo. Però vi garantisco che è un posto reale, una città reale. Magari proprio la vostra. Sto registrando questo nastro perché più gente possibile venga a conoscenza della verità. Forse, allora, la razza umana riuscirà a sopravvivere finché gli Andaliti torneranno ad aiutarci. Forse.",
                                    sTrama: "Protagonisti di Animorph sono cinque ragazzi (Jake, Rachel, Cassie, Tobias e Marco) che fortuitamente scoprono che la Terra sta subendo una silenziosa invasione aliena da parte degli Yeerk, una razza di esseri simili a lumaconi che hanno la capacità di avvolgersi intorno al cervello delle altre razze per assumere il controllo del loro corpo, trasformandole in creature chiamate Controller. I cinque vengono avvertiti del pericolo da un altro alieno, un Andalita, che dona loro una portentosa tecnologia che consente ai ragazzi di trasformarsi in qualunque animale tocchino. Il potere ha un'importante limitazione: se non si inverte la metamorfosi entro due ore, questa diventerà permanente. I cinque protagonisti, a cui si aggiungeranno altri personaggi, decidono di nominare se stessi 'Animorph'. Gli Animorph combattono gli Yeerk in incognito, senza mai rivelare il loro vero aspetto umano ai loro nemici che, per contro, si convincono di avere a che fare con dei banditi Andaliti, essendo questi ufficialmente l'unica specie in possesso del potere della metamorfosi.",
                                    sProtagonista: "Jake Berenson ha sempre dimostrato la sua maturità. Sin da ragazzino era quello che gli altri ammiravano e seguivano. Un leader. Concreto, responsabile, coraggioso e deciso. Ecco perché si è guadagnato il rispetto dei suoi compagni di battaglia, che sono portati spontaneamente ad affidarsi a lui nei momenti di maggior pericolo. Ma Jake accetta definitivamente il suo ruolo di capo degli Animorph soltanto quando scopre che suo fratello Tom è un Controller. Con l’obiettivo iniziale di liberare Tom dallo Yeerk che lo infesta Jake darà inizio alla logorante lotta contro i parassiti alieni e sarà costretto a prendere decisioni molto difficili.",
                                    sAmbientazione: "Le vicende di Animorph hanno inizio a New York, in un futuro molto prossimo. Una City che nei primi capitoli sarà proprio come quella che conosciamo, multietnica e cosmopolita. Ma la grande mela si trasformerà, in pochissimo tempo, nel primo, grande, campo di battaglia per la guerra tra yeerk ed esseri umani. E la frenesia ed il caos della metropoli lasceranno spazio a silenzio e desolazione, alternati a momenti di urla e terrore.",
                                    sImmagine: require('../../../assets/imgStorie/Animorph.jpg'),
                                    sVoto: 9,
                                })}>
                                    <Icon name='md-star' style={styles.iconStar} />
                                    <Text style={styles.txtStar}>Animorph</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {/*<View style={styles.viewTitolo}>
                        <Icon name='star' style={styles.iconTitolo} />
                        <Text style={styles.txtTitolo}>La maledizione della prima luna</Text>
                        </View>*/}
                        <View style={styles.viewIcone}>
                            <View style={styles.txtView}>
                                <Image source={require('../../../assets/img/favorite-4-64.png')} style={{ width: 50, height: 50 }} />
                                <Text style={styles.txtIcon}>2.318</Text>
                            </View>
                            <View style={styles.txtView}>
                                <Image source={require('../../../assets/img/yo.png')} style={{ width: 50, height: 50 }} />
                                <Text style={styles.txtIcon}>330</Text>
                            </View>
                            <View style={styles.txtView}>
                                <Image source={require('../../../assets/img/level.png')} style={{ width: 50, height: 50 }} />
                                <Text style={styles.txtIcon}>11</Text>
                            </View>
                        </View>
                        <View style={styles.viewFrase}>
                            {/* Adoro il cinema e ho visto tutti i film di Quentin Tarantino */}
                            <Text style={styles.txtFrase}>{this.props.profilo.sMotto}</Text>
                        </View>
                        {example2}
                        {example3}

                        <Separator bordered style={{ marginTop: 10, marginBottom: 10 }}>
                            <Text style={{ fontSize: 18, fontFamily: 'GoogleSans-Bold', textAlign: 'left' }}>Cronologia</Text>
                        </Separator>
                        {example4}
                        {/* {example5}                         */}
                    </Content>
                </Container>
            </SafeAreaView>
        );
    }
}

const ENTRIES1 = [
    {
        title: 'Shark – Il primo squalo (2018)',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/1QZhmuQ6WN5Hnr8IlbTGy9mCSAw.jpg'
    },
    {
        title: 'Mission: Impossible - Fallout (2018)',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/dFVDRjYY7nph90k3GXmyAJeVb5v.jpg'
    },
    {
        title: "Alpha: Un'amicizia forte come la vita (2018)",
        subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/2Pz9wIVwUiVhlLAJYae1irxzcTT.jpg'
    },
    {
        title: 'The Equalizer 2 - Senza Perdono (2018)',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/mpn1NjMNgafz7regWBdcbdLTA9Q.jpg'
    },
    {
        title: 'Maquia - When the Promised Flower Blooms (2018)',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/j3PR1Hifn8ACgtVADIMSNois9L3.jpg'
    },
    {
        title: 'Crazy & Rich (2018)',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/4PhEMamqnxUljPPMtwMj9SUqX3L.jpg'
    }
];

const ATTORI = [
    {
        title: 'Scarlett Johansson',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/tHMgW7Pg0Fg6HmB8Kh8Ixk6yxZw.jpg'
    },
    {
        title: 'Benedict Cumberbatch',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/wz3MRiMmoz6b5X3oSzMRC9nLxY1.jpg'
    },
    {
        title: "Jason Statham",
        subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/PhWiWgasncGWD9LdbsGcmxkV4r.jpg'
    },
    {
        title: 'Evangeline Lilly',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/uVNhCN2bgl0nyiA1OnNXB18inOi.jpg'
    },
    {
        title: 'Tom Hardy',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/4CR1D9VLWZcmGgh4b6kKuY2NOel.jpg'
    },
    {
        title: 'Cate Blanchett',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/5HikVWKfkkUa8aLdCMHtREBECIn.jpg'
    }
];

const CRONOLOGIA = [
    {
        title: 'Avengers: Infinity War (2018)',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/oqvx02LqhXcdlbFDhbMcuSFxzir.jpg'
    },
    {
        title: 'Solo: A Star Wars Story (2018)',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/pw8SERd6h2HrMFLPzODW1KnapoO.jpg'
    },
    {
        title: "Skyscraper (2018)",
        subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/5LYSsOPzuP13201qSzMjNxi8FxN.jpg'
    },
    {
        title: 'Hotel Transylvania 3 - Una vacanza mostruosa',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/1ET8YsbMDGgkrJ9QbOnHFOnlvGC.jpg'
    },
    {
        title: 'Ant-Man (2015)',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/gxWZLCzf64BQBBau8SpzCuSy3T6.jpg'
    },
    {
        title: 'Thor: Ragnarok (2017)',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/9k12tUa75kGKqyEe1Brdxyed0u1.jpg'
    }
];

const colors = {
    black: '#1a1917',
    gray: '#888888',
    background1: '#ffffff',
    background2: '#ffffff'
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        //backgroundColor: 'lightskyblue',
        height: 130,
        //paddingLeft: 5,
        //marginTop: 5,
    },
    viewIcone: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        height: 100,
        paddingLeft: 5,
        paddingTop: 5,
    },
    nome: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        paddingLeft: 4,
    },
    cognome: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        paddingLeft: 4,
    },
    username: {
        fontSize: 13,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        paddingLeft: 4,
        color: 'rgb(128,128,128)'
    },
    txtInteressi: {
        fontSize: 10,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        paddingLeft: 4,
    },
    txtInteressiWhite: {
        fontSize: 10,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        paddingLeft: 4,
        color: '#ffffff',
    },
    txtUtente: {
        flex: 1,
        width: 230,
        flexDirection: "row",
        textAlign: 'left',
        margin: 3,
        marginTop: 6,
    },
    txtView: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        margin: 15,
    },
    txtIconCateg: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        color: '#fff'
    },
    txtIcon: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
    },
    viewPicture: {
        width: 86,
        height: 86,
        backgroundColor: '#ffffff',
        borderRadius: 100,
        marginTop: 5,
        //position: 'absolute',
    },
    tmbProfile: {
        margin: 3,
    },
    viewInteressi: {
        alignItems: 'center',
        margin: 5,
        // width: 130,
        // borderWidth: 1,
        flex: 1,
    },
    viewStar: {
        flex: 1.5,
        flexDirection: 'column',
        alignItems: 'center',
        margin: 5,
        height: 85,
        overflow: 'hidden',
        // borderTopColor: 'rgb(128,128,128)',
        // borderWidth: 1,
    },
    txtStar: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 7,
        color: '#fff',
    },
    txtBtnModifica: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
    },
    iconTitolo: {
        fontSize: 40,
        textAlign: 'center',
        color: '#fff'
    },
    iconStar: {
        fontSize: 40,
        color: 'yellow',
        textAlign: 'center',
    },
    viewFrase: {
        alignItems: 'center',
        margin: 7,
        borderColor: 'rgb(128,128,128)',
        borderTopWidth: 1,
        borderBottomWidth: 1,
    },
    txtFrase: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginTop: 15,
        marginBottom: 15,
    },
    safeArea: {
        flex: 1,
    },
    gradient: {
        ...StyleSheet.absoluteFillObject
    },
    scrollview: {
        flex: 1
    },
    exampleContainer: {
        paddingVertical: 15
    },
    exampleContainerDark: {
        backgroundColor: colors.black
    },
    exampleContainerLight: {
        backgroundColor: 'white'
    },
    title: {
        paddingHorizontal: 15,
        backgroundColor: 'transparent',
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        flex: 5,
    },
    iconExit: {
        //position: 'absolute',
        fontSize: 35,
        color: '#808080',
        alignItems: 'center',
        marginRight: 5,
    },
    titleDark: {
        color: colors.black
    },
    subtitle: {
        marginTop: 5,
        paddingHorizontal: 30,
        backgroundColor: 'transparent',
        color: 'rgba(255, 255, 255, 0.75)',
        fontSize: 13,
        fontStyle: 'italic',
        textAlign: 'center'
    },
    slider: {
        marginTop: 15,
        overflow: 'visible' // for custom animations
    },
    sliderContentContainer: {
        paddingVertical: 10 // for custom animation
    },
    paginationContainer: {
        paddingVertical: 8
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 8
    }
});

function mapStateToProps(state) {
    return {
        profilo: state.profilo,
        ui: state.ui,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onInputChange: (profilo) => {
            dispatch(editProfilo(profilo));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfiloScreen);
