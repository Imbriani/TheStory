import React from 'react';
import ProfiloScreen from './ProfiloScreen';
import ModificaProfiloScreen from './EditProfiloScreen';
import ModificaTopTenScreen from './EditTopTenScreen';
import DettaglioTSScreen from '../tabTS/DettaglioTSScreen';

import { createStackNavigator } from 'react-navigation';

export default createStackNavigator({
    Profilo: {
        screen: ProfiloScreen,
        header: null,
        navigationOptions: {
            header: null
        }     
      },
      DettaglioTS: {
        screen: DettaglioTSScreen,  
        header: null,
        navigationOptions: {
            header: null
        }            
      },      
      ModificaProfilo: {
        screen: ModificaProfiloScreen,  
        header: null,
        navigationOptions: {
            header: null
        }            
      }, 
      ModificaTopTen: {
        screen: ModificaTopTenScreen,  
        header: null,
        navigationOptions: {
            header: null
        }            
      },                   
})