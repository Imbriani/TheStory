import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Alert, Dimensions, TextInput } from 'react-native';
import { Container, Content, Card, CardItem, Body, Right, Thumbnail, Icon, Left, Separator, Fab, Button } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-picker';

export default class tabEditProfilo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            idxInteresse: 1,
            sInteresse: 'Cinema',
            avatarSource: null,
            sNome: '',
            sCognome: '',
            sMotto: '',
        };
    }

    componentDidMount() {
        this.setState({
            avatarSource: this.props.navigation.getParam('PathImmagine'),
            idxInteresse: this.props.navigation.getParam('iInteresse'),
            sInteresse: this.props.navigation.getParam('sInteresse'),
            sNome: this.props.navigation.getParam('Nome'),
            sCognome: this.props.navigation.getParam('Cognome'),
            sMotto: this.props.navigation.getParam('Motto'),
        })
    }

    Salva =() => {        
        this.props.navigation.state.params.onSalva({
            Avatar: this.state.avatarSource,
            Nome: this.state.sNome,
            Cognome: this.state.sCognome,
            Motto: this.state.sMotto,
            Interesse: this.state.idxInteresse,
            InteresseTxt: this.state.sInteresse });

        this.props.navigation.goBack();
    }

    selezionaTipo = (iIdx, sTesto) => () => {
        this.setState({
            idxInteresse: iIdx,
            sInteresse: sTesto,
        });
    }

    delPhotos() {
        this.setState({
            avatarSource: null
        });
    }

    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.setState({
                    avatarSource: source
                });
            }
        });
    }

    render() {
        return (
            <Container>
                <KeyboardAwareScrollView>
                    <View style={styles.vwImmagine}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10 }}>
                            {this.state.avatarSource === null ?
                                <Text style={styles.txtImmagine}>Seleziona una foto per il profilo</Text>
                                :
                                <View>
                                    <View style={styles.viewPicture}>
                                        <Thumbnail large style={styles.tmbProfile} source={this.state.avatarSource} />
                                    </View>
                                    <Icon name='md-close-circle' style={styles.iconExit} onPress={() => { this.delPhotos() }} />
                                </View>
                            }
                        </View>
                        <View style={styles.vwButtonImg}>
                            <View style={{ position: 'absolute', top: 1, flexDirection: 'row' }}>
                                <Button rounded style={styles.btnCaricaImg} iconLeft onPress={() => { this.selectPhotoTapped() }}>
                                    <Icon name='ios-image-outline' style={{ fontSize: 25, color: '#000', marginLeft: 10, marginRight: 5 }}></Icon>
                                    <Text style={styles.txtButtonImg} uppercase={false}>Foto</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                    <View style={styles.vwScroll}>
                        <Button rounded style={[styles.btnTools, this.state.idxInteresse == 1 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(1, 'Cinema')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxInteresse == 1 ? styles.txtButtonToolsSel : {}]}>Cinema</Text>
                        </Button>
                        <Button rounded style={[styles.btnTools, this.state.idxInteresse == 2 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(2, 'Libri')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxInteresse == 2 ? styles.txtButtonToolsSel : {}]}>Libri</Text>
                        </Button>
                        <Button rounded style={[styles.btnTools, this.state.idxInteresse == 3 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(3, 'Cinema/Libri')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxInteresse == 3 ? styles.txtButtonToolsSel : {}]}>Tutto</Text>
                        </Button>
                    </View>
                    {/* <MultiSwitch currenteStatus={'Complete'} disableScroll={'true'}></MultiSwitch> */}
                    <View style={{ flex: 4 }}>                            
                        <TextInput style={[styles.txtInput, { borderBottomWidth: 1, borderColor: '#7a42f4' }]} placeholder='Nome' onChangeText={(sNome) => this.setState({sNome})}>
                            {this.state.sNome}
                        </TextInput>
                        <TextInput style={[styles.txtInput, { borderBottomWidth: 1, borderColor: '#7a42f4' }]} placeholder='Cognome' onChangeText={(sCognome) => this.setState({sCognome})}> 
                            {this.state.sCognome}                            
                        </TextInput>
                    </View>
                    <View style={styles.vwTesto}>                            
                        <TextInput style={styles.txtInputMotto} multiline={true} placeholder='Motto' onChangeText={(sMotto) => this.setState({sMotto})}>
                            {this.state.sMotto}
                        </TextInput>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center' }}>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center', }}>
                            <Button rounded style={styles.btnSalva} onPress={() => this.Salva()}>
                                <Text style={styles.txtButton} uppercase={false}>Ok</Text>
                            </Button>
                            <Button rounded style={styles.btnSalva} onPress={() => this.props.navigation.goBack()}>
                                <Text style={styles.txtButton} uppercase={false}>Annulla</Text>
                            </Button>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </Container >
        );
    }
}

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const styles = StyleSheet.create({
    vwScroll: {
        height: 40,
        flexDirection: 'row',
        borderBottomColor: '#7a42f4',
        borderBottomWidth: 1,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
    },
    btnSalva: {
        width: 100,
        height: 40,
        bottom: 10,
        alignItems: 'flex-end',
        flex: 1,
        justifyContent: 'flex-end',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 15,
    },
    txtButton: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
        bottom: 2,
    },
    btnTools: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
        flex: 1
    },
    btnToolsSel: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: 'rgba(63,81,181, .9)',
    },
    txtButtonTools: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        color: '#cdcdcd',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    txtButtonToolsSel: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Bold',
        color: '#fff',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    vwImmagine: {
        height: 150,
        borderBottomWidth: 1,
        borderBottomColor: '#7a42f4',
    },
    txtImmagine: {
        fontSize: 18,
        color: '#A9A9A9',
        fontFamily: 'GoogleSans-Regular',
        marginTop: 15,
        marginBottom: 55,
    },
    avatar: {
        borderRadius: 10,
        width: 150,
        height: 150,
        marginTop: 15
    },
    iconExit: {
        position: 'absolute',
        fontSize: 30,
        right: -35,
        top: 15,
        color: '#A9A9A9',
    },
    vwButtonImg: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
    },
    btnCaricaImg: {
        width: 120,
        height: 40,
        justifyContent: 'center',
        marginLeft: 15,
        backgroundColor: '#fff',
        borderColor: '#000'
    },
    txtButtonImg: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        color: '#000',
    },
    viewPicture: {
        width: 86,
        height: 86,
        backgroundColor: '#ffffff',
        borderRadius: 100,
        marginTop: 5,
        //position: 'absolute',
    },
    tmbProfile: {
        margin: 3,
    },
    txtInput: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        justifyContent: 'flex-start',
        margin: 5,
    },
    vwTesto: {
        height: 100,
        borderBottomWidth: 1,
        borderBottomColor: '#7a42f4',
        margin: 5,
    },
    txtInputMotto: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        justifyContent: 'flex-start',
    },
});
