import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, KeyboardAvoidingView, ScrollView, AsyncStorage } from 'react-native';
import { Button, Icon, Container, Item, Input, Form, Label } from 'native-base';

export default class Welcome extends React.Component {
    
    signIn = async() => {
        await AsyncStorage.setItem('userToken', 'varun')

        this.props.navigation.navigate('App')
    }

    render() {
        return (
            <Container>                
                <ScrollView contentContainerStyle={{ flexGrow: 1}} scrollEnabled={false}>
                    <KeyboardAvoidingView behavior="padding" style={{ flex: 1, bottom: 0}}>
                        <ImageBackground source={require('../../assets/img/BGLogin2.jpg')} style={styles.container}>
                        </ImageBackground>
                        
                        <View style={styles.viewBG}>
                            <View style={styles.viewLogo}>                                
                                <Image source={require('../../assets/img/Logo.png')} style={{ width: 100, height: 119 }} />
                            </View>
                            <View style={styles.viewInput}>
                                <Form>
                                    <Item floatingLabel style={styles.itmLogin}>
                                        <Label style={styles.lblItem}>E-Mail</Label>
                                        <Input style={styles.inputItem} keyboardType='email-address'/>
                                    </Item>
                                    <Item floatingLabel style={styles.itmLogin}>
                                        <Label style={styles.lblItem}>Password</Label>
                                        <Input style={styles.inputItem} secureTextEntry={true}/>
                                    </Item>
                                    <Item floatingLabel style={styles.itmLogin}>
                                        <Label style={styles.lblItem}>Conferma Password</Label>
                                        <Input style={styles.inputItem} secureTextEntry={true}/>
                                    </Item>                                    
                                    <View style={styles.viewAccedi}>
                                        <Button rounded style={styles.btnLogin} onPress={this.signIn}>
                                            <Text style={styles.txtBtnLogin} >Registrati</Text>
                                        </Button>
                                        <Text style={styles.txtPwdDimenticata} >Non hai ricevuto l'e-mail di registrazione?</Text>
                                    </View>
                                </Form>

                            </View>
                            <View style={styles.viewButtonSocial}>
                                <View>
                                    <Button rounded style={styles.btnSocial} onPress={this.signIn}>
                                        <Icon name='logo-google' />
                                        <Text style={styles.txtButton} >Accedi con Google</Text>
                                    </Button>
                                    <Button rounded style={styles.btnSocial} onPress={this.signIn}>
                                        <Icon name='logo-facebook' />
                                        <Text style={styles.txtButton} >Accedi con Facebook</Text>
                                    </Button>
                                </View>
                            </View>
                        </View>
                        <Icon name='md-close-circle' style={styles.iconExit} onPress={() => this.props.navigation.goBack()}/>                        
                    </KeyboardAvoidingView>
                </ScrollView>
            </Container >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
    },
    viewBG: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(47,79,79, .6)',
        flexDirection: 'column',
    },
    viewLogo: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        margin: 15,
    },
    iconExit:{
        position: 'absolute',
        fontSize: 35,
        left: 10,
        top: 10,
        color: '#ffffff',
    },
    viewInput: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: 'rgb(255,255,255)',
        width: '100%',
    },
    viewButtonSocial: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
        marginTop: 5,
        width: 240,
    },
    txtButton: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        margin: 5,
        left: 2,
    },
    itmLogin: {
        margin: 5,
    },
    lblItem:{
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
    },
    inputItem:{
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
    },
    btnLogin: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        width: 100,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtBtnLogin: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
    },    
    txtPwdDimenticata: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        color: '#000000',
        textAlign: 'left',
        marginLeft: 10,
        maxWidth: 190,
    },
    viewAccedi: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    btnSocial: {
        width: 240,
        height: 53,
        margin: 8,
    }
});

