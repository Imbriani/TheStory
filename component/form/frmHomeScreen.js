import React, { Component } from 'react';
import { StyleSheet, View, Image, AsyncStorage, StatusBar } from 'react-native';
import { Container, Header, Tabs, Tab, TabHeading, Icon, Text, Footer, FooterTab, Button, ScrollableTab, Title, Body, Left, Right} from 'native-base';
import TabTS from './tabTS/tabTS';
import TabHome from './tabHome/tabHome';
import TabProfilo from './viewProfilo/tabProfilo';
import TabMenu from './tabMenu';
import Modal from 'react-native-modalbox';

export default class HomeScreen extends Component {

  static navigationOptions = {
    header: null,
    title: 'The Story',    
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#3F51B5',
    },
    headerTitleStyle: {
      fontSize: 25,
      fontFamily: 'GoogleSans-Bold',
      textAlign: 'center',
      alignSelf: 'center',
      flex: 1
    }
  };

  constructor(props) {
    super(props);
    this.state = {
    };
    this.onPressBtnFabio = this.onPressBtnFabio.bind(this);
    this.SignUp = this.SignUp.bind(this);
  }

  onClose() {
    console.log('Modal just closed');
  }

  onOpen() {
    console.log('Modal just opened');
  }

  onClosingState(state) {
    console.log('the open/close of the swipeToClose just changed');
  }

  onPressBtnFabio(navTxt){
    this.props.navigation.navigate(navTxt);
    this.refs.modFabio.close();
  }

  SignUp = async()=>{
    AsyncStorage.clear()
    console.log('signup')
    this.props.navigation.navigate('AuthLoading')
  }  

  render() {
    return (
      <Container style={styles.container} >
      <StatusBar barStyle='light-content' backgroundColor="#1565c0" />
       {/*} <Header hasTabs >
          <Left style={styles.left} />
          <Body style={styles.body}>
            <Title style={styles.bodyText}>The Story</Title>
          </Body>
          <Right style={styles.right} />
    </Header> */}
        {/*<Tabs renderTabBar={() => <ScrollableTab />}>*/}
        <Tabs locked={true}>
          <Tab heading={<TabHeading><Text style={styles.tabTextTS}>TS</Text></TabHeading>}>
          {/* <Tab heading={<TabHeading><Image source={require('../../assets/img/tabLogo.png')} style={{ width: 25, height: 38}} /></TabHeading>}>           */}
            <TabTS />
          </Tab>
          <Tab heading={<TabHeading><Icon name='ios-home-outline' style={{fontSize: 40, textAlign: 'center'}} /><Text style={styles.tabText}></Text></TabHeading>}>
            <TabHome />
          </Tab>
          <Tab heading={<TabHeading><Icon name='ios-person-outline' style={{fontSize: 40, textAlign: 'center'}}/><Text style={styles.tabText}></Text></TabHeading>}>
            <TabProfilo />
          </Tab>
          <Tab heading={<TabHeading><Icon name='ios-apps-outline' style={{fontSize: 40, textAlign: 'center'}}/></TabHeading>}>
            <TabMenu onPress={this.SignUp.bind(this)} />
          </Tab>
        </Tabs>

        <Footer>
          <FooterTab style={{backgroundColor: '#F5F5F5'}}>
            <Button onPress={() => this.onPressBtnFabio('navCerca')}>
              <Icon name='ios-search-outline' style={{color: '#808080', fontSize: 30,}}/>
            </Button>
            <Button onPress={() => this.refs.modFabio.open()}>
              <Icon name='ios-add-circle-outline' style={{color: '#808080', fontSize: 30,}}/>
            </Button>
          </FooterTab>
        </Footer>

        <Modal style={[styles.modal, styles.modFabio]} position={"bottom"} swipeToClose={false} ref={"modFabio"}>
          <View style={styles.divFabio}>
            <Button style={styles.btnFabio} onPress={() => this.onPressBtnFabio('navCondividi')}>
              {/* <Image source={require('../../assets/img/pen.png')} style={{ width: 30, height: 30 }} /> */}
              <Icon name='ios-create-outline' style={styles.iconText}/>
              <Text style={styles.fabioText} uppercase={false}>Condividi</Text>
            </Button>
            <Button style={styles.btnFabio} onPress={() => this.onPressBtnFabio('navTrova')}>
              {/* <Image source={require('../../assets/img/question.png')} style={{ width: 30, height: 30 }} /> */}
              <Icon name='ios-help-circle-outline' style={styles.iconText}/>
              <Text style={styles.fabioText} uppercase={false}>Trova</Text>
            </Button>
            <Button style={styles.btnFabio} onPress={() => this.onPressBtnFabio('navDiscussione')}>
              {/* <Image source={require('../../assets/img/star.png')} style={{ width: 50, height: 50 }} /> */}
              {/* <Image source={require('../../assets/img/chat.png')} style={{ width: 30, height: 30 }} /> */}
              <Icon name='ios-chatbubbles-outline' style={styles.iconText}/>
              <Text style={styles.fabioText} uppercase={false}>Apri Discussione</Text>
            </Button>
          </View>
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
  },
  tabText: {
    fontFamily: 'GoogleSans-Regular',
  },
  tabTextTS: {
    fontSize: 35,
    fontFamily: 'GoogleSans-Bold',
  },
  left: {
    flex: 1,
  },
  body: {
    flex: 1,
  },
  bodyText: {
    fontSize: 25,
    fontFamily: 'GoogleSans-Bold',
    justifyContent: 'center',
    marginLeft: 5,
  },
  right: {
    flex: 1,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modFabio: {
    height: 115,
  },
  fabioText: {
    fontSize: 10,
    fontFamily: 'GoogleSans-Regular',
    color: '#808080',
  },
  iconText: {
    fontSize: 30,
    color: '#808080'
  },  
  divFabio: {
    flex: 2,
    flexDirection: 'row',
    backgroundColor: '#F5F5F5',
  },
  btnFabio: {
    flex: 1,
    height: 55,
    flexDirection: 'column',  
    margin: 2, 
    backgroundColor: '#F5F5F5',   
  },
});
