import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity, StatusBar } from 'react-native';
import { Button, Icon, Container, Item, Input, Form, Label } from 'native-base';

export default class Welcome extends React.Component {

    render() {
        return (
            <Container>
            <ImageBackground source={require('../../assets/img/BGLogin2.jpg')} style={styles.container}>
                <StatusBar barStyle='light-content' backgroundColor="#1565c0" />
                <View style={styles.viewBG}>
                    <View style={styles.viewLogo}>
                        <Image source={require('../../assets/img/Logo.png')} style={{ width: 150, height: 178 }} />
                    </View>
                    <View style={styles.viewTextClaim}>
                        <Text style={styles.txtClaim}>
                            Scegli tu la prossima grande storia da raccontare
                        </Text>
                    </View>
                    <View style={styles.viewButton}>
                        <Button rounded style={styles.btnSocial} onPress={() => this.props.navigation.navigate('Login')}>
                            <Text style={styles.txtButton} >Accedi</Text>
                        </Button>
                        {/*<Button style={styles.btnLogin} title='Inizia' onPress={() => this.props.navigation.navigate('SignIn')}></Button>*/}
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SignIn')}>
                            <Text>
                                <Text style={styles.txtAccedi}>
                                    Non hai un account?
                                </Text>
                                <Text style={styles.txtAccedi2}> Inizia!</Text>
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    viewBG: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(47,79,79, .6)',
        flexDirection: 'column',
    },
    viewLogo: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        margin: 15,
    },
    viewTextClaim: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20,
    },
    txtClaim: {
        fontSize: 30,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
    },
    viewButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 15,
    },
    btnSocial: {
        width: 240,
        height: 53,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',        
    },   
    txtButton: {
        fontSize: 25,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center'
    },     
    btnLogin: {
        padding: 15,
        marginBottom: 8,
        width: 90,
        fontSize: 35,
        fontFamily: 'GoogleSans-Bold',
    },
    txtAccedi: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
        margin: 15,
        marginTop: 8,
    },
    txtAccedi2: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        color: '#fff',
        textAlign: 'center',
        margin: 15,
        marginLeft: 10,
    }
});
