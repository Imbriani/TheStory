import React, { Component } from 'react';
import { StyleSheet, FlatList, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Container, Text, List, ListItem, View, Icon, Button } from "native-base";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class tabCerca extends Component {

    constructor(props) {
        super(props);
        this.state = {
            idxTipoDisc: 5,
            sTestoFiltro: 'Inserisci il titolo di un Film',

            films: [],
            sApiFilm: 'https://api.themoviedb.org/3/search/movie?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=',
        };
        console.log('cons');
    }

    selezionaTipo = (iIdx, sTesto) => () => {
        this.setState({
            idxTipoDisc: iIdx,
            sTestoFiltro: sTesto,
            query: '',
            films: [],
        });
        console.log(this.state.query);
        switch (iIdx) {
            case 5:
                this.setState({ sApi: 'https://api.themoviedb.org/3/search/movie?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=', });
                break;
            case 6:
                this.setState({ sApi: 'https://api.themoviedb.org/3/search/tv?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=', });
                break;
            case 1:
                this.setState({ sApi: 'https://api.themoviedb.org/3/search/person?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=', });
                break;
            case 3:
                this.setState({ sApi: 'https://api.themoviedb.org/3/search/person?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=', });
                break;
        }
        console.log(this.state.sApi);
    }

    ResetFiltro = () => () => {
        this.setState({
            idxTipoDisc: 5,
            sTestoFiltro: 'Inserisci il titolo di un Film',
            films: [],
            sApiFilm: 'https://api.themoviedb.org/3/search/movie?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=',
        });
    }
    // ResetFiltro = () => this.setState(state => ({
    //     idxTipoDisc: 5,
    //     sTestoFiltro: 'Inserisci il titolo di un Film',
    //     films: [],
    //     sApiFilm: 'https://api.themoviedb.org/3/search/movie?api_key=7cd360888450c7a73c00aa699702d975&language=it-IT&page=1&include_adult=false&query=',
    // }));

    render() {

        return (

            <Container>
                <View style={styles.vwScroll}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ alignContent: 'center' }}>
                        <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 5 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(5, 'Inserisci il titolo di un Film')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 5 ? styles.txtButtonToolsSel : {}]}>Film</Text>
                        </Button>
                        <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 6 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(6, 'Inserisci il titolo di una Serie TV')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 6 ? styles.txtButtonToolsSel : {}]}>Serie TV</Text>
                        </Button>
                        <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 1 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(1, 'Inserisci il nome di un Attore')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 1 ? styles.txtButtonToolsSel : {}]}>Attore</Text>
                        </Button>
                        <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 2 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(2, 'Inserisci il nome di uno Scrittore')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 2 ? styles.txtButtonToolsSel : {}]}>Scrittore</Text>
                        </Button>
                        <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 3 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(3, 'Inserisci il nome di un Regista')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 3 ? styles.txtButtonToolsSel : {}]}>Regista</Text>
                        </Button>
                        <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 4 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(4, 'Inserisci il titolo di un Libro')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 4 ? styles.txtButtonToolsSel : {}]}>Libro</Text>
                        </Button>
                        <Button rounded style={[styles.btnTools, this.state.idxTipoDisc == 4 ? styles.btnToolsSel : {}]} onPress={this.selezionaTipo(7, 'Inserisci il titolo di una TS')}>
                            <Text uppercase={false} style={[styles.txtButtonTools, this.state.idxTipoDisc == 7 ? styles.txtButtonToolsSel : {}]}>TS</Text>
                        </Button>
                    </ScrollView>
                </View>

                <View style={{ alignItems: 'center', flexDirection: 'row', minHeight: 45, height: 55, borderBottomWidth: 1, borderBottomColor: '#CED0CE' }}>
                    <TextInput placeholder={this.state.sTestoFiltro} style={styles.txtInput}
                        onChangeText={text =>
                            fetch(`${this.state.sApiFilm}${text}`)
                                .then((response) => response.json())
                                .then((responseJson) => {
                                    console.log(responseJson);
                                    this.setState({
                                        films: responseJson,
                                    }, function () {
                                    });
                                })
                                .catch((error) => {
                                    console.error(error);
                                })
                        }>
                    </TextInput>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: .8, marginRight: 5 }} onPress={this.ResetFiltro}>
                        <Icon name='md-close-circle' style={styles.iconExit} />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 5, }}>
                    <FlatList
                        keyboardShouldPersistTaps='handled'
                        style={{ height: 40, flex: 1 }}
                        data={this.state.films.results}
                        renderItem={({ item }) =>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity style={{ flex: 4 }} onPress={() => this.props.navigation.navigate('TabDettaglio', {
                                    sTitolo: item.title,
                                    sPoster: `https://image.tmdb.org/t/p/original${item.poster_path}`,
                                    sTrailer: item.overview,
                                })}>
                                    <Text style={styles.txtDescrizione}>{item.title} ({item.release_date})</Text>
                                </TouchableOpacity>
                                <View style={{ flex: .3, alignItems: 'flex-end', justifyContent: 'center' }} >
                                    <Icon name="ios-arrow-forward-outline" style={{ color: '#CED0CE', fontSize: 35, marginRight: 5 }} />
                                </View>
                            </View>
                        }
                        ItemSeparatorComponent={() =>
                            <View
                                style={{
                                    height: 2,
                                    backgroundColor: "#CED0CE",
                                }}
                            />
                        }
                        keyExtractor={item => { item.title - item.release_date }}
                    />
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    Text: {
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'GoogleSans-Bold',
    },
    vwScroll: {
        height: 40,
        minHeight: 40,
        flexDirection: 'row',
        borderBottomColor: '#7a42f4',
        borderBottomWidth: 1,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
    },
    txtInput: {
        fontSize: 15,
        textAlign: 'left',
        justifyContent: 'center',
        fontFamily: 'GoogleSans-Regular',
        margin: 4,
        borderWidth: 1,
        borderRadius: 20,
        borderColor: '#7a42f4',
        paddingHorizontal: 10,
        flex: 6,
    },
    iconExit: {
        //position: 'absolute',
        fontSize: 35,
        color: '#808080',
        alignItems: 'center',
        marginRight: 5,
    },
    txtDescrizione: {
        fontSize: 16,
        textAlign: 'left',
        justifyContent: 'center',
        fontFamily: 'GoogleSans-Regular',
        margin: 7,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    iconText: {
        fontSize: 30,
        color: '#808080'
    },
    btnSalva: {
        width: 100,
        height: 30,
        bottom: 10,
        alignItems: 'flex-end',
        flex: 1,
        justifyContent: 'flex-end',
        marginLeft: 5,
        marginRight: 5,
    },
    txtButton: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
        bottom: 2,
    },
    list: {
        paddingRight: 7,
        paddingLeft: 7,
        margin: 0,
        borderWidth: 0,
        maxHeight: 270, // necessary to make scrolling of list possible see:
    },
    itemText: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
    },
    btnTools: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
    },
    btnToolsSel: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: 'rgba(63,81,181, .9)',
    },
    txtButtonTools: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        color: '#cdcdcd',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    txtButtonToolsSel: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
});
