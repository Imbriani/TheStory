import React from 'react';
import CercaScreen from './TestataCercaScreen';
import DettaglioScreen from './DettaglioCercaScreen';

import { createStackNavigator } from 'react-navigation';

export default createStackNavigator({
    TabCerca: {
        screen: CercaScreen,        
        header: null,
        navigationOptions: {
            header: null
        }  
    },
    TabDettaglio: {
        screen: DettaglioScreen,
        header: null,
        navigationOptions: {
            header: null
        }
    }, 
})