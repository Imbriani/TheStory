import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Alert, Dimensions, Image } from 'react-native';
import { Container, Content, Card, CardItem, Body, Right, Thumbnail, Icon, Left, Separator, Fab, Button } from 'native-base';

export default class tabDettaglioCerca extends Component {
    
    componentWillMount(){ 
        console.log(this.props.navigation.getParam('sPoster'));
    }

    constructor(props) {
        super(props);
        this.state = {
            // valoreVoto: 'Vota'
        };
    }

    Voto = (iVoto) => () => {
        this.setState({
            valoreVoto: iVoto,            
        });
        this.refs.modVoto.close()
    }

    render() {
        return (
            <Container>
                <Content padder>
                    <Card>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row', position: 'absolute', top: 0, left: 0, right: 0, height: 110, backgroundColor: '#1565c0', borderBottomWidth: 1, borderColor: 'rgb(128,128,128)' }} >
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, flexDirection: 'column', }}>
                                    <Text style={styles.CardTextHeader}>{this.props.navigation.getParam('sTitolo')}</Text>                               

                                    {/* <View style={{ flex: 1, flexDirection: 'row', marginTop: 3}}>
                                        <View style={{ flex: 1, marginRight: 3, alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={styles.viewTipo}>
                                                <Text style={styles.textTipo}>Avventure</Text>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={styles.viewTipo}>
                                                <Text style={styles.textTipo}>Fantasy</Text>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1, marginLeft: 3, alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={styles.viewTipo}>
                                                <Text style={styles.textTipo}>Crime</Text>
                                            </View>
                                        </View>
                                    </View> */}
                                </View>
                            </View>  
                            <Thumbnail large style={styles.tmbProfile} source={{uri: this.props.navigation.getParam('sPoster') }} />
                            <Icon name='md-close-circle' style={styles.iconExit} onPress={() => this.props.navigation.goBack()}/>
                        </CardItem>                   
                        

                        {/* TRAMA */}
                        <Separator bordered style={{ marginTop: 3 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'GoogleSans-Bold', textAlign: 'left' }}>Trama</Text>
                        </Separator>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View>
                                    <Text style={styles.CardTextDetail} note>{this.props.navigation.getParam('sTrailer')}</Text>
                                </View>
                            </View>
                        </CardItem>  
                    </Card>
                </Content >
            </Container >
        );
    }
}

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    iconExit: {
        position: 'absolute',
        fontSize: 35,
        right: 5,
        top: 7,
        color: '#ffffff',
        flex: 1,
        alignItems: 'flex-end',
    },
    modal: {
        height: 115,
        flex: 1,
        flexDirection: 'column',
        borderWidth: .4,
        borderColor: 'rgb(47,79,79)',
        backgroundColor: '#FFFFFF'
    },
    divVotoScarso: {
        backgroundColor: '#f44336',
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 3,
        marginRight: 3,
    },
    divVotoMediocre: {
        backgroundColor: '#ffee58',
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 3,
        marginRight: 3,
    },
    divVotoBuono: {
        backgroundColor: '#9ccc65',
        flex: 1,
        flexDirection: 'row',
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 3,
        marginRight: 3,
    },
    txtVotoFloat: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        justifyContent: 'center',
    },
    txtVotoFloatScarso: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        justifyContent: 'center',
        color: '#DCDCDC',
    },    
    CardTextHeader: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'left',
        marginLeft: 70,
        color: '#fff',
        maxHeight: 50,
        minHeight: 50,
        maxWidth: viewportWidth - 150,
    },
    CardTextHeaderNote: {
        fontSize: 13,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 5,
        marginBottom: 2,
    },
    txtSegui: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        color: '#fff'
    },
    txtAvviaDiscussione: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        color: '#fff',
    },
    txtVotoMedio: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
        marginRight: 5,
        color: '#fff',
    },
    txtVoto: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'center',
        margin: 8,
        marginTop: 17,
    },
    viewTipo: {

        marginTop: 3,
        borderRadius: 70,
        borderColor: 'rgb(128,128,128)',
        borderRadius: 50,
        borderWidth: 1,
        alignItems: 'center',
        width: '100%'
    },
    textTipo: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
        marginRight: 5,
    },
    viewDiscussione: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        height: 40,
        borderTopWidth: 1,
        borderColor: '#808080',
        alignItems: 'center',
    },
    txtNDiscussioni: {
        fontSize: 17,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
    },
    txtAudio: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 8,
        color: '#C0C0C0',
    },
    CardTextDetail: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'justify',
        marginRight: 5,
    },
    tmbProfile: {
        borderWidth: 1,
        borderColor: '#fff',
        top: 3,
        left: 3,
        // marginTop: 1,
        // marginBottom: 5,
        position: 'absolute',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    popOverStyle: {
        backgroundColor: '#ecf0f1',
    },
    activeTabStyle: {
        backgroundColor: '#3F51B5'
    },
    viewIcone: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        height: 100,
        paddingLeft: 5,
        paddingTop: 5,
        borderBottomWidth: 1,
    },
    txtView: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        margin: 15,
    },
    iconFiltro: {
        fontSize: 30,
        textAlign: 'center',
    },
    txtIcon: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
    },
});
