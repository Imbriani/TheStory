import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, ScrollView, Image, Dimensions } from 'react-native';
import { Container, Content, Card, CardItem, Thumbnail, Icon, Text, Footer, FooterTab, Button, ScrollableTab, Title, Body, Left, Right } from 'native-base';


export default class followScreen extends Component {

    render() {
        return (
            <Container>
                <Content padder>
                    <View style={styles.txtHeadRilevanti}>
                        <Text style={styles.txtHeadRilevanti}>Ecco le discussioni più rilevanti della settimana</Text>
                        <TouchableOpacity style={{alignItems: 'flex-end', marginRight: 5 }}>
                            <Icon name='ios-albums-outline' style={{ fontSize: 30, color: '#696969' }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ height: 195 }}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ marginTop: 3 }}>
                            <View style={styles.DiscRilevanti}>
                                <View style={styles.vwTopRilevanti} >
                                    <Text style={styles.CardTextHeaderUtente}>Ciccio Bozzo</Text>
                                    <Text style={styles.CardTextHeader}>Alter Ego: Memorie di un viaggiatore ultracorporeo</Text>
                                </View>

                                <View style={styles.vwTxtDescRilevanti} >
                                    <Text style={styles.txtDescRilevanti}>A me è piaciuto molto, secondo voi potrebbe esserci un sequel?</Text>
                                </View>

                                <View style={styles.vwBottomRilevanti}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={styles.txtBtnUnisciti}>Unisciti</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.DiscRilevanti}>
                                <View style={styles.vwTopRilevanti} >
                                    <Text style={styles.CardTextHeaderUtente}>Francesco Ciccarese</Text>
                                    <Text style={styles.CardTextHeader}>Alter Ego: Memorie di un viaggiatore ultracorporeo</Text>
                                </View>

                                <View style={styles.vwTxtDescRilevanti} >
                                    <Text style={styles.txtDescRilevanti}>Ma alla fine il potere del cervo da dove arriva?</Text>
                                </View>

                                <View style={styles.vwBottomRilevanti}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={styles.txtBtnUnisciti}>Unisciti</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.DiscRilevanti}>
                                <View style={styles.vwTopRilevanti} >
                                    <Text style={styles.CardTextHeaderUtente}>Gabriele Litti</Text>
                                    <Text style={styles.CardTextHeader}>Animorph</Text>
                                </View>

                                <View style={styles.vwTxtDescRilevanti} >
                                    <Text style={styles.txtDescRilevanti}>Questo libro mi piace talmente tanto che lo sto pubblicizzando a destra e a manca</Text>
                                </View>

                                <View style={styles.vwBottomRilevanti}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={styles.txtBtnUnisciti}>Unisciti</Text>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                    <Card>
                        <CardItem>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 74, backgroundColor: '#1565c0', borderBottomWidth: 1, borderBottomColor: 'rgb(128,128,128)' }} >
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#808080', borderBottomWidth: 1, left: 1 }}>
                                <Thumbnail style={styles.tmbProfile} source={require('../../../assets/img/Lucia.jpg')} />
                                <View style={{ flex: 1, flexDirection: 'column' }} button onPress={() => Alert.alert('hi')}>
                                    <Text style={styles.CardTextHeaderUtente}>Lucia Verdi</Text>
                                    <Text style={styles.CardTextHeader}>Sta cercando un libro. Aiutala per ricevere 5 YO!</Text>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Text style={styles.CardTextDetail} note>Cerco qualcosa di diverso, un romanzo che faccia riflettere...</Text>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={styles.viewDiscussione}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../../assets/img/discussione2.png')} style={{ width: 20, height: 20 }} />
                                        <Text style={styles.txtNDiscussioni}>73</Text>
                                    </View>
                                    <TouchableOpacity>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                            <Icon name='ios-thumbs-up-outline' style={{ fontSize: 20, color: '#696969' }} />
                                            <Text style={styles.txtNDiscussioni}>23</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 74, backgroundColor: '#1565c0', borderBottomWidth: 1, borderBottomColor: 'rgb(128,128,128)' }} >
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', borderColor: '#808080', borderBottomWidth: 1, left: 1 }}>
                                <Thumbnail style={styles.tmbProfile} source={require('../../../assets/img/Mario.jpg')} />
                                <View style={{ flex: 1, flexDirection: 'column' }} button onPress={() => Alert.alert('hi')}>
                                    <Text style={styles.CardTextHeaderUtente}>Mario Rossi</Text>
                                    <Text style={styles.CardTextHeader}>Si è unito a una discussione su "Inception"</Text>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Text style={styles.CardTextDetail} note>Un Di Caprio esemplare</Text>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style={styles.viewDiscussione}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={require('../../../assets/img/discussione2.png')} style={{ width: 20, height: 20 }} />
                                        <Text style={styles.txtNDiscussioni}>73</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DiscussioniAni')}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                            <Icon name='ios-chatbubbles-outline' style={{ fontSize: 25, color: '#696969' }} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </CardItem>
                    </Card>                    
                </Content>
            </Container>
        );
    }
}

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const styles = StyleSheet.create({
    vwContainer: {
        flex: 2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    DiscRilevanti: {
        borderWidth: 1,
        margin: 10,
        width: 210,
        borderRadius: 15,
        flex: 3,
        flexDirection: 'column',
    },
    vwTxtHeadRilevanti: {
        height: 25,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtHeadRilevanti: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        alignItems: 'center',
        marginLeft: 3,
        marginBottom: 3,
        flexDirection: 'row',
        flex: 1,
    },
    vwTopRilevanti: {
        top: 0,
        left: 0,
        right: 0,
        height: 10,
        backgroundColor: '#1565c0',
        borderWidth: 2,
        borderColor: '#1565c0',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        flex: 2,
        flexDirection: 'column',
    },
    vwTxtDescRilevanti: {
        flex: 2,
        flexDirection: 'column',
    },
    txtDescRilevanti: {
        fontSize: 12,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 7,
        marginTop: 5,
    },
    vwBottomRilevanti: {
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#1565c0',
    },
    txtBtnUnisciti: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'left',
        marginTop: 5,
        color: '#fff',
    },
    CardTextHeaderUtente: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Bold',
        textAlign: 'left',
        marginLeft: 7,
        marginRight: 7,
        color: '#fff',
        marginTop: 3,
    },
    CardTextHeader: {
        fontSize: 12,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        marginLeft: 7,
        marginRight: 7,
        color: '#fff',
        maxHeight: 30,
        minHeight: 30,
    },
    txtNDiscussioni: {
        fontSize: 12,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginRight: 5,
    },
    viewDiscussione: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        height: 40,
        borderTopWidth: 1,
        borderColor: '#808080',
        alignItems: 'center',
    },
    txtNDiscussioni: {
        fontSize: 14,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        marginLeft: 5,
    },    
});
