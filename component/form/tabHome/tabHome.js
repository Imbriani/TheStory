import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, AsyncStorage, Image } from 'react-native';
import { Container, Header, Tabs, Tab, TabHeading, Icon, Text, Footer, FooterTab, Button, ScrollableTab, Title, Body, Left, Right} from 'native-base';
import TabFollow from './FollowScreen';
import TabMondo from './MondoScreen';

export default class tabHome extends Component {

  render() {
    return (
      <Container style={styles.container} >
        <Tabs locked={true} tabBarUnderlineStyle={{borderBottomWidth: 2, borderColor: '#808080'}} tabContainerStyle={{ height: 80 }}>
          <Tab heading={
            <TabHeading style={styles.tabStyle}>
                <Icon name='ios-people-outline' style={{fontSize: 40, textAlign: 'center', color: '#808080'}} />
                <Text style={styles.tabText}>I tuoi follow</Text>
            </TabHeading>}>
            <TabFollow />
          </Tab>
          <Tab heading={
            <TabHeading style={styles.tabStyle}>
                <Icon name='ios-globe-outline' style={{fontSize: 40, textAlign: 'center', color: '#808080'}} />
                <Text style={styles.tabText}>Mondo</Text></TabHeading>}>
            <TabMondo />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
  },
  tabStyle:{
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F5F5F5',
    height: 80,
  }, 
  tabText: {
    fontFamily: 'GoogleSans-Regular',
    color: '#808080',
  },
  tabTextTS: {
    fontSize: 35,
    fontFamily: 'GoogleSans-Bold',
  },
  left: {
    flex: 1,
  },
  body: {
    flex: 1,
  },
  bodyText: {
    fontSize: 25,
    fontFamily: 'GoogleSans-Bold',
    justifyContent: 'center',
    marginLeft: 5,
  },
  right: {
    flex: 1,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modFabio: {
    height: 115,
  },
  fabioText: {
    fontSize: 11,
    fontFamily: 'GoogleSans-Regular',
    color: '#808080',
  },
  iconText: {
    fontSize: 30,
    color: '#808080'
  },  
  divFabio: {
    flex: 2,
    flexDirection: 'row',
    backgroundColor: '#F5F5F5',
  },
  btnFabio: {
    flex: 1,
    height: 55,
    flexDirection: 'column',  
    margin: 2, 
    backgroundColor: '#F5F5F5',   
  },
});
