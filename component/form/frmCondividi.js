import React, { Component } from 'react';
import { StyleSheet, TextInput, PermissionsAndroid, Image, ScrollView } from 'react-native';
import { Container, Text, View, Icon, Button } from "native-base";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-picker';
import Tags from "react-native-tags";

export default class frmCondividi extends Component {

    static navigationOptions = {
        title: 'Condividi',
        headerTintColor: '#ffffff',
        headerStyle: {
            backgroundColor: '#3F51B5',
        },
        headerTitleStyle: {
            fontSize: 20,
            fontFamily: 'GoogleSans-Regular',
        }
    };

    state = {
        avatarSource: null,
        videoSource: null
    };

    delPhotos(){
        this.setState({
            avatarSource: null
        });        
    }

    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.setState({
                    avatarSource: source
                });
            }
        });
    }
    selectVideoTapped() {
        const options = {
            title: 'Video Picker',
            takePhotoButtonTitle: 'Take Video...',
            mediaType: 'video',
            videoQuality: 'medium'
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled video picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                this.setState({
                    videoSource: response.uri
                });
            }
        });
    }

    async requestExternalStoreageRead() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                {
                    'title': 'Cool App ...',
                    'message': 'App needs access to external storage'
                }
            );

            return granted == PermissionsAndroid.RESULTS.GRANTED
        }
        catch (err) {
            //Handle this error
            return false;
        }
    }

    getPhotos = async () => {
        //Before calling getPhotos, request permission
        if (await this.requestExternalStoreageRead()) {
            console.log('getfoto');
            CameraRoll.getPhotos({
                first: 10,
                assetType: 'All'
            })
                .then((r) => {
                    console.log({ r });
                    this.setState({ photos: r.edges, summary: `Number of photos found ${r.edges.length}` });
                })
                .catch((error) => {
                    this.setState({ errorMsg: error.message });
                })
        }
    }

    render() {
        return (

            <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }} style={{ flex: 1 }} scrollEnabled={false}>
                <Container>
                    <View style={styles.vwIntestazione}>
                        {/* <Text style={styles.text}>Usa la @ per taggare un film\libro\attore</Text> */}
                        <Text style={styles.text}>Tags:</Text>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                            <Tags
                                initialTags={[]}
                                onChangeTags={tags => console.log(tags)}
                                onTagPress={(index, tagLabel, event, deleted) =>
                                    console.log(index, tagLabel, event, deleted ? "deleted" : "not deleted")
                                }
                                containerStyle={{ justifyContent: "center" }}
                                inputStyle={[styles.txtTag, { backgroundColor: "white", borderWidth: 1, borderColor: '#D3D3D3' }]}
                                tagTextStyle={styles.txtTag}
                            />
                            <Text style={styles.textInfo}>Usa lo spazio per creare il tag</Text>
                        </ScrollView>

                    </View>

                    <View style={styles.vwTesto}>
                        <TextInput style={styles.txtTesto} multiline={true} placeholder='Testo/link...'>

                        </TextInput>
                    </View>
                    <View style={styles.vwImmagine}>
                        <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
                            {this.state.avatarSource === null ? <Text style={styles.txtImmagine}>Seleziona una foto</Text> : 
                            <View>
                                <Image style={styles.avatar} source={this.state.avatarSource} />
                                <Icon name='md-close-circle' style={styles.iconExit} onPress={() => { this.delPhotos()}}/>
                            </View>
                            }
                        </View>
                        <View style={styles.vwButtonImg}>
                            <View style={{ position: 'absolute', top: 7, flexDirection: 'row' }}>
                                <Button rounded style={styles.btnCaricaImg} iconLeft onPress={() => { this.selectPhotoTapped() }}>
                                    <Icon name='ios-image-outline' style={{ fontSize: 25, color: '#000', marginLeft: 10 }}></Icon>
                                    <Text style={styles.txtButtonImg} uppercase={false}>Foto</Text>
                                </Button>
                                {/* <Button rounded style={styles.btnCaricaImg} iconLeft onPress={() => { this.selectVideoTapped() }}>
                                    <Icon name='ios-videocam-outline' style={{ fontSize: 25, color: '#000', marginLeft: 10 }}></Icon>
                                    <Text style={styles.txtButtonImg} uppercase={false}>Video</Text>
                                </Button> */}
                            </View>
                        </View>
                    </View>
                    <View style={styles.vwButton}>
                        <View style={{ position: 'absolute', top: 7 }}>
                            <Button rounded style={styles.btnPubblica}>
                                <Text style={styles.txtButton} uppercase={false}>Pubblica</Text>
                            </Button>
                        </View>
                    </View>
                </Container>
            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        margin: 10,
        color: '#A9A9A9'
    },
    textInfo: {
        fontSize: 12,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'center',
        margin: 10,
        color: '#A9A9A9'
    },
    vwIntestazione: {
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#7a42f4',
        marginLeft: 5,
        marginRight: 5,
    },
    txtTag: {
        fontFamily: 'GoogleSans-Regular',
        fontSize: 18,
    },
    vwTesto: {
        flex: 2,
        borderBottomWidth: 1,
        borderBottomColor: '#7a42f4',
        marginLeft: 5,
        marginRight: 5,
    },
    txtTesto: {
        fontSize: 18,
        fontFamily: 'GoogleSans-Regular',
        textAlign: 'left',
        justifyContent: 'flex-start',
        paddingLeft: 10,
        paddingRight: 10,
    },
    vwImmagine: {
        flex: 2,
        borderBottomWidth: 1,
        borderBottomColor: '#7a42f4',
    },
    vwButton: {
        flex: 1.1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: 20,
    },
    btnPubblica: {
        width: 200,
        height: 50,
        justifyContent: 'center',
    },
    txtButton: {
        fontSize: 20,
        fontFamily: 'GoogleSans-Regular',
        color: '#fff',
    },
    vwButtonImg: {
        flex: .8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    txtImmagine: {
        fontSize: 18,
        color: '#A9A9A9',
        fontFamily: 'GoogleSans-Regular',
    },
    btnCaricaImg: {
        width: 120,
        height: 40,
        justifyContent: 'center',
        marginLeft: 15,
        backgroundColor: '#fff',
        borderColor: '#000'
    },
    txtButtonImg: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Regular',
        color: '#000',
    },
    avatar: {
        borderRadius: 10,
        width: 150,
        height: 150,
        marginTop: 15
    },
    iconExit: {
        position: 'absolute',
        fontSize: 30,
        right: -35,
        top: 15,
        color: '#A9A9A9',
    },
});
